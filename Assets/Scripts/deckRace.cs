﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class deckRace : MonoBehaviour {

	public persistentObjects decks_scripts;

	public int my_id = 0;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
	}
	
	// Update is called once per frame
	public void SetSelectedRace(){

		decks_scripts.setSelectedRace (my_id);
		SceneManager.LoadScene ("newDeckHeroes");
	}
}
