﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class decksScripts : MonoBehaviour {

	GameObject[] all_slots;

	// Use this for initialization
	void Start () {
		all_slots = GameObject.FindGameObjectsWithTag ("deckSlot");
		Array.Sort (all_slots, delegate(GameObject slot1, GameObject slot2) { return slot1.name.CompareTo(slot2.name); });

		for (int x = 0; x < all_slots.Length; x++) {
			all_slots [x].GetComponent<deckSlot> ().updateGraphics ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			SceneManager.LoadScene ("mainMenu");
	}

	public void BackPressed(){
		SceneManager.LoadScene ("mainMenu");
	}

}
