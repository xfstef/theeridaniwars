﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class persistentObjects : MonoBehaviour {

	public static persistentObjects Instance;

	void Awake ()   
	{
		if (Instance == null)
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy (gameObject);
		}
	}

	Sprite frame_humans, frame_plati, bkg_humans, bkg_plati, selected_humans, selected_plati, card_back_humans, 
	card_back_plati, empty_battle_slot, battle_frame_humans, battle_frame_plati;
	Sprite[] human_heroes, plati_heroes, human_special_units, plati_special_units, human_units, plati_units, 
	human_specials, plati_specials, human_spells, plati_spells, deck_frames, hero_portraits, bf_circles, effect_icons;

	static int selected_slot = 0;	// From 1 to 16.
	static int selected_race = 0;	// From 1 to 2.
	static int selected_hero = 0;	// From 1 to 12.
	static int selected_sac = 0;	// From 1 to 18.
	static bool new_deck_mode;
	static deck[] player_decks;

	card[] human_hero_cards = new card[6];
	card[] plati_hero_cards = new card[6];
	card[] human_s_u_cards = new card[6];
	card[] plati_s_u_cards = new card[6];
	card[] human_unit_cards = new card[10];
	card[] plati_unit_cards = new card[10];
	card[] human_spell_cards = new card[5];
	card[] plati_spell_cards = new card[5];
	card[] human_s_a_cards = new card[9];
	card[] plati_s_a_cards = new card[9];

	static player player_save;
	BinaryFormatter formatter;
	FileStream saveFile;

	skill[] all_skills = new skill[28];
	public skillLibrary skill_library;

	// Use this for initialization
	void Start () {
		frame_humans = Resources.Load("HUMANS M", typeof(Sprite)) as Sprite;
		frame_plati = Resources.Load("PLATI (2)", typeof(Sprite)) as Sprite;
		bkg_humans = Resources.Load("Menu/PHumans", typeof(Sprite)) as Sprite;
		bkg_plati = Resources.Load("Menu/pplati2", typeof(Sprite)) as Sprite;
		selected_humans = Resources.Load ("Menu/pAtrat", typeof(Sprite)) as Sprite;
		selected_plati = Resources.Load ("Menu/pAtrat2", typeof(Sprite)) as Sprite;
		card_back_humans = Resources.Load("Menu/BACK CARDh", typeof(Sprite)) as Sprite;
		card_back_plati = Resources.Load("Menu/BACK CARDp", typeof(Sprite)) as Sprite;
		empty_battle_slot = Resources.Load("Menu/EmtiSpace1", typeof(Sprite)) as Sprite;
		battle_frame_humans = Resources.Load("ramaMicaHumans", typeof(Sprite)) as Sprite;
		battle_frame_plati = Resources.Load("ramaMicaPlati", typeof(Sprite)) as Sprite;

		human_heroes = Resources.LoadAll<Sprite> ("Characters/HHeroes");
		plati_heroes = Resources.LoadAll<Sprite> ("Characters/PHeroes");
		human_special_units = Resources.LoadAll<Sprite> ("Characters/HSpecialUnits");
		plati_special_units = Resources.LoadAll<Sprite> ("Characters/PSpecialUnits");
		human_units = Resources.LoadAll<Sprite> ("Characters/HUnits");
		plati_units = Resources.LoadAll<Sprite> ("Characters/PUnits");
		human_specials = Resources.LoadAll<Sprite> ("Characters/HSpecials");
		plati_specials = Resources.LoadAll<Sprite> ("Characters/PSpecials");
		human_spells = Resources.LoadAll<Sprite> ("Characters/HSpells");
		plati_spells = Resources.LoadAll<Sprite> ("Characters/PSpells");
		deck_frames = Resources.LoadAll<Sprite> ("deks H&P");
		hero_portraits = Resources.LoadAll<Sprite> ("Characters/AllHerosFace");
		bf_circles = Resources.LoadAll<Sprite> ("Menu/Cercuri");
		effect_icons = Resources.LoadAll<Sprite> ("Menu/PASIVALL");

		skill temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Reinforced Plates");
		temp_skill.setSkillID (100);
		temp_skill.setST (true);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (15);	// Hero bonus armos points.
		temp_skill.setEffectIcon(0);	// Needed if the skill requires an effect icon.
		all_skills [0] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Martial Pose");
		temp_skill.setSkillID (101);
		temp_skill.setST (true);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (16);	// Hero melee attack may not trigger retaliation.
		temp_skill.setEffectIcon(1);	// Needed if the skill requires an effect icon.
		all_skills [1] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Cover Me!");
		temp_skill.setSkillID (102);
		temp_skill.setST (true);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (10);	// Hero death ward.
		temp_skill.setEffectIcon(6);	// Needed if the skill requires an effect icon.
		all_skills [2] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Subversion");
		temp_skill.setSkillID (103);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (45);	// Damage debuff.
		all_skills [3] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Into the Shadows");
		temp_skill.setSkillID (104);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (11);	// Set invisibility to self.
		all_skills [4] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Critical Strikes");
		temp_skill.setSkillID (105);
		temp_skill.setST (true);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (17);	// Hero gets a critical strike (+50% damage) chance of 20%.
		temp_skill.setEffectIcon(2);	// Needed if the skill requires an effect icon.
		all_skills [5] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Clever Micro-Bots");
		temp_skill.setSkillID (106);
		temp_skill.setST (false);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (31);	//	Periodic heal to all robots.
		temp_skill.setEffectIcon(3);
		all_skills [6] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Static discharge");
		temp_skill.setSkillID (107);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (42);	// Extra retaliation damage + extra effect against mechanical.
		all_skills [7] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Acidic Shells");
		temp_skill.setSkillID (108);
		temp_skill.setST (false);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (12);	// Hero Target receives one extra point of damage from other ranged attacks for one turn.
		temp_skill.setEffectIcon(4);	// Needed if the skill requires an effect icon.
		all_skills [8] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Holy Fusion");
		temp_skill.setSkillID (109);
		temp_skill.setST (false);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (51);	// All current units get Hollow Brother respawn but lose one health point.
		temp_skill.setEffectIcon(14);	// Needed if the skill requires an effect icon.
		all_skills [9] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Ageless Blade");
		temp_skill.setSkillID (110);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (13);	// Gives bonus to attack but causes self harm on cast.
		all_skills [10] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand's Murmur");
		temp_skill.setSkillID (111);
		temp_skill.setST (true);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (14);	// Hero gains extra retaliation damage.
		temp_skill.setEffectIcon(11);	// Needed if the skill requires an effect icon.
		all_skills [11] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Slaver's Will");
		temp_skill.setSkillID (112);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (50);	// Summons a friendly random Hollow One.
		all_skills [12] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Tricks of the Trade");
		temp_skill.setSkillID (113);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (60);	// Melee Invincibility for one round.
		temp_skill.setEffectIcon(10);	// Needed if the skill requires an effect icon.
		all_skills [13] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand Whipper");
		temp_skill.setSkillID (114);
		temp_skill.setST (false);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (70);	// Hero gains Hollow One tamer ability + Hollow Ones get +attack for each extra Whip Master on the field.
		temp_skill.setEffectIcon(16);	// Needed if the skill requires an effect icon.
		all_skills [14] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Super Heated Steam");
		temp_skill.setSkillID (115);
		temp_skill.setST (false);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (43);	// Steam Units gain +2 attack.
		temp_skill.setEffectIcon(12);	// Needed if the skill requires an effect icon.
		all_skills [15] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Book of Wise Techno Sayings");
		temp_skill.setSkillID (116);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (71);	// Choose between +attack, +health or +less of both.
		all_skills [16] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand Treated edges");
		temp_skill.setSkillID (117);
		temp_skill.setST (false);
		temp_skill.setPassive (true);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (44);	// All melee units gain extra damage on their first strike.
		temp_skill.setEffectIcon(9);	// Needed if the skill requires an effect icon.
		all_skills [17] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Emergency Health Pack");
		temp_skill.setSkillID (200);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (20);	// Direct heal to full health.
		all_skills [18] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Tactical Retreat");
		temp_skill.setSkillID (201);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (72);	// Unit retreats back to hand at the end of the turn.
		temp_skill.setEffectIcon(5);	// Needed if the skill requires an effect icon.
		all_skills [19] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Uplifting Speech");
		temp_skill.setSkillID (202);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (52);	// Unit skips ahead and moves next.
		all_skills [20] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Genius Foresight");
		temp_skill.setSkillID (203);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (80);	// Draw 3 cards. Pick one for your hand. The rest go back in the deck.
		all_skills [21] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Nano-Bot Eye Upgrade");
		temp_skill.setSkillID (204);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (40);	//	%Chance to crit added. 
		temp_skill.setEffectIcon(7);	// Needed if the skill requires an effect icon.
		all_skills [22] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand Bandage");
		temp_skill.setSkillID (205);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (true);
		temp_skill.setTriggerID (30);	// Small heal over time to one target.
		temp_skill.setEffectIcon(13);	// Needed if the skill requires an effect icon.
		all_skills [23] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Deathly Howl");
		temp_skill.setSkillID (206);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (61);	// Permanent Taunt.
		temp_skill.setEffectIcon(15);	// Needed if the skill requires an effect icon.
		all_skills [24] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Bargain");
		temp_skill.setSkillID (207);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (62);	// Target skips its turn automatically.
		temp_skill.setEffectIcon(8);	// Needed if the skill requires an effect icon.
		all_skills [25] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand Sacrifice");
		temp_skill.setSkillID (208);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (81);	// Select a card to destroy / discard and summon a random unit from your deck either to the battlefield or in hand.
		all_skills [26] = temp_skill;

		temp_skill = gameObject.AddComponent<skill> ();
		temp_skill.setSkillName ("Sand Infusion");
		temp_skill.setSkillID (209);
		temp_skill.setST (true);
		temp_skill.setPassive (false);
		temp_skill.setRecurring (false);
		temp_skill.setTriggerID (63);	// The next hero attack will stun the target for one turn.
		temp_skill.setEffectIcon(17);	// Needed if the skill requires an effect icon.
		all_skills [27] = temp_skill;

		card temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1100);
		temp_card.setName ("Blake");
		temp_card.setFullName ("First Officer Blake");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (0);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setHeroPortraitID (2);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (25);
		temp_card.setDamage (4);
		human_hero_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1101);
		temp_card.setName ("Jenny May");
		temp_card.setFullName ("Ruthless Jenny May");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (0);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setHeroPortraitID (7);
		temp_card.setRange (1);
		temp_card.setInitiative (3000);
		temp_card.setHealth (20);
		temp_card.setDamage (5);
		human_hero_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1102);
		temp_card.setName ("J'or");
		temp_card.setFullName ("Counselor J'or");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (1);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (2);
		temp_card.setHeroPortraitID (11);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (22);
		temp_card.setDamage (6);
		human_hero_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1103);
		temp_card.setName ("Igrette");
		temp_card.setFullName ("Death's Whisper Igrette");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (1);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (3);
		temp_card.setHeroPortraitID (3);
		temp_card.setRange (2);
		temp_card.setInitiative (2000);
		temp_card.setHealth (18);
		temp_card.setDamage (6);
		human_hero_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1104);
		temp_card.setName ("Parker");
		temp_card.setFullName ("Doctor Parker");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (2);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (4);
		temp_card.setHeroPortraitID (6);
		temp_card.setRange (1);
		temp_card.setInitiative (2000);
		temp_card.setHealth (20);
		temp_card.setDamage (5);
		human_hero_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1105);
		temp_card.setName ("Jeff");
		temp_card.setFullName ("Senior SysAdmin Jeff");
		temp_card.setRace (1);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (2);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (5);
		temp_card.setHeroPortraitID (10);
		temp_card.setRange (0);
		temp_card.setInitiative (4000);
		temp_card.setHealth (20);
		temp_card.setDamage (7);
		human_hero_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2100);
		temp_card.setName ("Kuza");
		temp_card.setFullName ("High Priestess Kuza");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (0);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setHeroPortraitID (0);
		temp_card.setRange (1);
		temp_card.setInitiative (2000);
		temp_card.setHealth (20);
		temp_card.setDamage (3);
		plati_hero_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2101);
		temp_card.setName ("Mahar");
		temp_card.setFullName ("Tomb Watcher Mahar");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (0);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setHeroPortraitID (1);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (24);
		temp_card.setDamage (4);
		plati_hero_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2102);
		temp_card.setName ("Malur");
		temp_card.setFullName ("RakShan (Prince) Matur");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (1);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (2);
		temp_card.setHeroPortraitID (8);
		temp_card.setRange (1);
		temp_card.setInitiative (3000);
		temp_card.setHealth (18);
		temp_card.setDamage (7);
		plati_hero_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2103);
		temp_card.setName ("Prayer");
		temp_card.setFullName ("Prayer");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (1);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (3);
		temp_card.setHeroPortraitID (5);
		temp_card.setRange (0);
		temp_card.setInitiative (4000);
		temp_card.setHealth (20);
		temp_card.setDamage (8);
		plati_hero_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2104);
		temp_card.setName ("Galkivar");
		temp_card.setFullName ("Big Brother Galkivar");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (2);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (4);
		temp_card.setHeroPortraitID (4);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (21);
		temp_card.setDamage (6);
		plati_hero_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2105);
		temp_card.setName ("Turumn");
		temp_card.setFullName ("Brother Turumn");
		temp_card.setRace (2);
		temp_card.setCardType (1);
		temp_card.setHeroClassOffset (2);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (5);
		temp_card.setHeroPortraitID (9);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (22);
		temp_card.setDamage (4);
		plati_hero_cards [5] = temp_card;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1400);
		temp_card.setName ("R. Plates");
		temp_card.setFullName ("Reinforced Plates");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [0]);
		human_s_a_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1401);
		temp_card.setName ("Martial Pose");
		temp_card.setFullName ("Martial Pose");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [1]);
		human_s_a_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1402);
		temp_card.setName ("Cover Me!");
		temp_card.setFullName ("Command - Cover Me!");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (2);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [2]);
		human_s_a_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1403);
		temp_card.setName ("Subversion");
		temp_card.setFullName ("Subversion");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (3);
		temp_card.setSkill (all_skills [3]);
		human_s_a_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1404);
		temp_card.setName ("To Shadows");
		temp_card.setFullName ("Into the Shadows");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (4);
		temp_card.setSkill (all_skills [4]);
		human_s_a_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1405);
		temp_card.setName ("Crit Strikes");
		temp_card.setFullName ("Critical Strikes");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (5);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [5]);
		human_s_a_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1406);
		temp_card.setName ("Micro-Bots");
		temp_card.setFullName ("Clever Micro-Bots");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (6);
		temp_card.setCardCode (6);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [6]);
		human_s_a_cards [6] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1407);
		temp_card.setName ("Discharge");
		temp_card.setFullName ("Static Discharge");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (7);
		temp_card.setCardCode (7);
		temp_card.setSkill (all_skills [7]);
		human_s_a_cards [7] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1408);
		temp_card.setName ("Acid Shells");
		temp_card.setFullName ("Weapon Upgrade - Acidic Shells");
		temp_card.setRace (1);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (8);
		temp_card.setCardCode (8);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [8]);
		human_s_a_cards [8] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2400);
		temp_card.setName ("Holy Fusion");
		temp_card.setFullName ("Holy Fusion");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (0);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [9]);
		plati_s_a_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2401);
		temp_card.setName ("Ageless Blade");
		temp_card.setFullName ("Ageless Blade");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (1);
		temp_card.setSkill (all_skills [10]);
		plati_s_a_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2402);
		temp_card.setName ("Murmur");
		temp_card.setFullName ("Sand's Murmur");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (2);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [11]);
		plati_s_a_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2403);
		temp_card.setName ("Slaver's Will");
		temp_card.setFullName ("Slaver's Will");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (3);
		temp_card.setSkill (all_skills [12]);
		plati_s_a_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2404);
		temp_card.setName ("Tricks");
		temp_card.setFullName ("Tricks of the Trade");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (4);
		temp_card.setSkill (all_skills [13]);
		plati_s_a_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2405);
		temp_card.setName ("Sand Whipper");
		temp_card.setFullName ("Sand Whipper");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (5);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [14]);
		plati_s_a_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2406);
		temp_card.setName ("S.H. Steam");
		temp_card.setFullName ("Super Heated Steam");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (6);
		temp_card.setCardCode (6);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [15]);
		plati_s_a_cards [6] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2407);
		temp_card.setName ("B.W.T.S.");
		temp_card.setFullName ("Book of Wise Techno Sayings");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (7);
		temp_card.setCardCode (7);
		temp_card.setSkill (all_skills [16]);
		plati_s_a_cards [7] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2408);
		temp_card.setName ("S.T. Edges");
		temp_card.setFullName ("Weapon Upgrade - Sand Treated Edges");
		temp_card.setRace (2);
		temp_card.setCardType (4);
		temp_card.setCardGraphic (8);
		temp_card.setCardCode (8);
		temp_card.setIsPassive (true);
		temp_card.setSkill (all_skills [17]);
		plati_s_a_cards [8] = temp_card;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1500);
		temp_card.setName ("Health Pack");
		temp_card.setFullName ("Emergency Health Pack");
		temp_card.setRace (1);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setSkill (all_skills [18]);
		human_spell_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1501);
		temp_card.setName ("Retreat");
		temp_card.setFullName ("Tactical Retreat");
		temp_card.setRace (1);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (1);
		temp_card.setSkill (all_skills [19]);
		human_spell_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1502);
		temp_card.setName ("Speech");
		temp_card.setFullName ("Uplifting Speech");
		temp_card.setRace (1);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (2);
		temp_card.setSkill (all_skills [20]);
		human_spell_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1503);
		temp_card.setName ("Foresight");
		temp_card.setFullName ("Tactical Genius Foresight");
		temp_card.setRace (1);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (3);
		temp_card.setSkill (all_skills [21]);
		human_spell_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1504);
		temp_card.setName ("Eye Upgrade");
		temp_card.setFullName ("Nano-Bot Eye Upgrade");
		temp_card.setRace (1);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (4);
		temp_card.setSkill (all_skills [22]);
		human_spell_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2500);
		temp_card.setName ("Bandage");
		temp_card.setFullName ("Sand Bandage");
		temp_card.setRace (2);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setSkill (all_skills [23]);
		plati_spell_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2501);
		temp_card.setName ("Howl");
		temp_card.setFullName ("Deathly Howl");
		temp_card.setRace (2);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setSkill (all_skills [24]);
		plati_spell_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2502);
		temp_card.setName ("Bargain");
		temp_card.setFullName ("Bargain");
		temp_card.setRace (2);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (2);
		temp_card.setSkill (all_skills [25]);
		plati_spell_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2503);
		temp_card.setName ("Sacrifice");
		temp_card.setFullName ("Sand Sacrifice");
		temp_card.setRace (2);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (3);
		temp_card.setSkill (all_skills [26]);
		plati_spell_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2504);
		temp_card.setName ("Infusion");
		temp_card.setFullName ("Sand Infusion");
		temp_card.setRace (2);
		temp_card.setCardType (5);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (4);
		temp_card.setSkill (all_skills [27]);
		plati_spell_cards [4] = temp_card;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1300);
		temp_card.setName ("Deck Hand");
		temp_card.setFullName ("Deck Hand");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (7);
		temp_card.setCardCode (0);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (3);
		temp_card.setDamage (2);
		human_unit_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1301);
		temp_card.setName ("Clean'O'Bot");
		temp_card.setFullName ("Clean'O'Bot");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (2);
		temp_card.setDamage (3);
		human_unit_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1302);
		temp_card.setName ("Automaticom");
		temp_card.setFullName ("Automaticom");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (2);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (3);
		temp_card.setDamage (2);
		human_unit_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1303);
		temp_card.setName ("H. Mechanic");
		temp_card.setFullName ("Hydraulics Mechanic");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (6);
		temp_card.setCardCode (3);
		temp_card.setRange (0);
		temp_card.setInitiative (1000);
		temp_card.setHealth (3);
		temp_card.setDamage (3);
		human_unit_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1304);
		temp_card.setName ("Grumpy Cook");
		temp_card.setFullName ("Grumpy Cook");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (4);
		temp_card.setRange (1);
		temp_card.setInitiative (2000);
		temp_card.setHealth (1);
		temp_card.setDamage (4);
		human_unit_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1305);
		temp_card.setName ("Lab Intern");
		temp_card.setFullName ("Lab Intern");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (5);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (2);
		temp_card.setDamage (2);
		human_unit_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1306);
		temp_card.setName ("AutoFlat");
		temp_card.setFullName ("AutoFlat");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (6);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (5);
		temp_card.setDamage (0);
		human_unit_cards [6] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1307);
		temp_card.setName ("Green Recruit");
		temp_card.setFullName ("Green Recruit");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (7);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (2);
		temp_card.setDamage (3);
		human_unit_cards [7] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1308);
		temp_card.setName ("Ship Gunner");
		temp_card.setFullName ("Ship Gunner");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (9);
		temp_card.setCardCode (8);
		temp_card.setRange (2);
		temp_card.setInitiative (0);
		temp_card.setHealth (2);
		temp_card.setDamage (2);
		human_unit_cards [8] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1309);
		temp_card.setName ("M. Mechanic");
		temp_card.setFullName ("Oilthumb Master Mechanic");
		temp_card.setRace (1);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (8);
		temp_card.setCardCode (9);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (2);
		temp_card.setDamage (1);
		human_unit_cards [9] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2300);
		temp_card.setName ("Fire Adept");
		temp_card.setFullName ("Fire Adept");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (0);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (4);
		temp_card.setDamage (1);
		plati_unit_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2301);
		temp_card.setName ("Tinkerer");
		temp_card.setFullName ("Tinkerer");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (1);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (3);
		temp_card.setDamage (2);
		plati_unit_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2302);
		temp_card.setName ("Whip Master");
		temp_card.setFullName ("Whip Master");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (2);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (3);
		temp_card.setDamage (1);
		plati_unit_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2303);
		temp_card.setName ("Solemn");
		temp_card.setFullName ("Solemn");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (3);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (3);
		temp_card.setDamage (2);
		plati_unit_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2304);
		temp_card.setName ("Hollow Brother");
		temp_card.setFullName ("Hollow One - Brother");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (4);
		temp_card.setRange (0);
		temp_card.setInitiative (2000);
		temp_card.setHealth (2);
		temp_card.setDamage (3);
		plati_unit_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2305);
		temp_card.setName ("Hollow Sister");
		temp_card.setFullName ("Hollow One - Sister");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (6);
		temp_card.setCardCode (5);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (3);
		temp_card.setDamage (2);
		plati_unit_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2306);
		temp_card.setName ("Pit Fighter");
		temp_card.setFullName ("Pit Fighter");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (9);
		temp_card.setCardCode (6);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (2);
		temp_card.setDamage (3);
		plati_unit_cards [6] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2307);
		temp_card.setName ("Enthusiast");
		temp_card.setFullName ("Enthusiast");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (7);
		temp_card.setCardCode (7);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (1);
		temp_card.setDamage (4);
		plati_unit_cards [7] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2308);
		temp_card.setName ("Bok Master");
		temp_card.setFullName ("Bok Kennel Master");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (8);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (2);
		temp_card.setDamage (2);
		plati_unit_cards [8] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2309);
		temp_card.setName ("Sand Caster");
		temp_card.setFullName ("Sand Caster");
		temp_card.setRace (2);
		temp_card.setCardType (3);
		temp_card.setCardGraphic (8);
		temp_card.setCardCode (9);
		temp_card.setRange (1);
		temp_card.setInitiative (0);
		temp_card.setHealth (4);
		temp_card.setDamage (2);
		plati_unit_cards [9] = temp_card;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1200);
		temp_card.setName ("War Hog");
		temp_card.setFullName ("War Hog");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (0);
		temp_card.setRange (1);
		temp_card.setInitiative (0);
		temp_card.setHealth (7);
		temp_card.setDamage (2);
		human_s_u_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1201);
		temp_card.setName ("Riot Officer");
		temp_card.setFullName ("Riot Forces Officer");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (1);
		temp_card.setRange (0);
		temp_card.setInitiative (1000);
		temp_card.setHealth (8);
		temp_card.setDamage (3);
		human_s_u_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1202);
		temp_card.setName ("Operative");
		temp_card.setFullName ("Operative");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (2);
		temp_card.setRange (2);
		temp_card.setInitiative (1000);
		temp_card.setHealth (4);
		temp_card.setDamage (4);
		human_s_u_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1203);
		temp_card.setName ("Infiltrator");
		temp_card.setFullName ("Tactical Infiltrator");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (3);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (5);
		temp_card.setDamage (6);
		human_s_u_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1204);
		temp_card.setName ("A. Officer");
		temp_card.setFullName ("Artillery Officer");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (4);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (5);
		temp_card.setDamage (3);
		human_s_u_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (1205);
		temp_card.setName ("C.O.R.E.");
		temp_card.setFullName ("Calibrations Officer for Repairs and Engineering");
		temp_card.setRace (1);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (5);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (5);
		temp_card.setDamage (3);
		human_s_u_cards [5] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2200);
		temp_card.setName ("Wind Charmer");
		temp_card.setFullName ("Sand Keeper - Wind Charmer");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (3);
		temp_card.setCardCode (0);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (5);
		temp_card.setDamage (3);
		plati_s_u_cards [0] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2201);
		temp_card.setName ("Oath Keeper");
		temp_card.setFullName ("Oath Keeper");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (5);
		temp_card.setCardCode (1);
		temp_card.setRange (0);
		temp_card.setInitiative (1000);
		temp_card.setHealth (8);
		temp_card.setDamage (3);
		plati_s_u_cards [1] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2202);
		temp_card.setName ("Pit Lord");
		temp_card.setFullName ("Pit Fights Lord");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (4);
		temp_card.setCardCode (2);
		temp_card.setRange (0);
		temp_card.setInitiative (3000);
		temp_card.setHealth (5);
		temp_card.setDamage (6);
		plati_s_u_cards [2] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2203);
		temp_card.setName ("Awoken One");
		temp_card.setFullName ("Hollow One - The Awoken");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (2);
		temp_card.setCardCode (3);
		temp_card.setRange (1);
		temp_card.setInitiative (2000);
		temp_card.setHealth (4);
		temp_card.setDamage (5);
		plati_s_u_cards [3] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2204);
		temp_card.setName ("Ranger");
		temp_card.setFullName ("Brotherhood - Ranger");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (0);
		temp_card.setCardCode (4);
		temp_card.setRange (1);
		temp_card.setInitiative (2000);
		temp_card.setHealth (4);
		temp_card.setDamage (5);
		plati_s_u_cards [4] = temp_card;

		temp_card = gameObject.AddComponent<card> ();
		temp_card.setCardID (2205);
		temp_card.setName ("Suppressor");
		temp_card.setFullName ("Brotherhood - Suppressor");
		temp_card.setRace (2);
		temp_card.setCardType (2);
		temp_card.setCardGraphic (1);
		temp_card.setCardCode (5);
		temp_card.setRange (1);
		temp_card.setInitiative (1000);
		temp_card.setHealth (5);
		temp_card.setDamage (3);
		plati_s_u_cards [5] = temp_card;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		player_save = new player ();
		loadDecks ();
		player_decks = player_save.getDecks ();
	}

	public player getPlayerSave(){
		return player_save;
	}

	public void loadDecks(){
		BinaryFormatter bf = new BinaryFormatter ();
		if (File.Exists (Application.persistentDataPath + "save.binary")) {
			FileStream file = File.Open (Application.persistentDataPath + "save.binary", FileMode.Open);
			player_save = (player)bf.Deserialize (file);
			file.Close ();
		} else {
			//Directory.CreateDirectory ("Saves");
			player_save.initializeEmpty ();
			saveDecks ();
		}
	}

	public void saveDecks(){
		// Code for saving / serializing a file.
		//formatter = new BinaryFormatter ();
		//File.Delete ("Saves/save.binary");
		//saveFile = File.Create ("Saves/save.binary");
		//saveFile = File.Open ("Saves/save.binary", FileMode.Create);
		//formatter.Serialize (saveFile, player_save);
		//saveFile.Close ();

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "save.binary");
		bf.Serialize(file, player_save);
		file.Close();
	}

	public card retrieveCard(int card_id){
		switch (card_id / 1000) {	// Parsing race.
		case 1:	// Humans.
			switch (card_id / 100) {	// Parsing card type.
			case 11:	//	Heroes.
				for (int x = 0; x < human_hero_cards.Length; x++) {
					if ((card_id % 100) == human_hero_cards [x].getCardCode ())
						return human_hero_cards [x];
				}
				break;
			case 12:	// Special Units.
				for (int x = 0; x < human_s_u_cards.Length; x++) {
					if ((card_id % 100) == human_s_u_cards [x].getCardCode ())
						return human_s_u_cards [x];
				}
				break;
			case 13:	// Units.
				for (int x = 0; x < human_unit_cards.Length; x++) {
					if ((card_id % 100) == human_unit_cards [x].getCardCode ())
						return human_unit_cards [x];
				}
				break;
			case 14:	// Special Abilities.
				for (int x = 0; x < human_s_a_cards.Length; x++) {
					if ((card_id % 100) == human_s_a_cards [x].getCardCode ())
						return human_s_a_cards [x];
				}
				break;
			case 15:	// Abilities.
				for (int x = 0; x < human_spell_cards.Length; x++) {
					if ((card_id % 100) == human_spell_cards [x].getCardCode ())
						return human_spell_cards [x];
				}
				break;
			}
			break;
		case 2:	// Plati.
			switch (card_id / 100) {	// Parsing card type.
			case 21:	//	Heroes.
				for (int x = 0; x < plati_hero_cards.Length; x++) {
					if ((card_id % 100) == plati_hero_cards [x].getCardCode ())
						return plati_hero_cards [x];
				}
				break;
			case 22:	// Special Units.
				for (int x = 0; x < plati_s_u_cards.Length; x++) {
					if ((card_id % 100) == plati_s_u_cards [x].getCardCode ())
						return plati_s_u_cards [x];
				}
				break;
			case 23:	// Units.
				for (int x = 0; x < plati_unit_cards.Length; x++) {
					if ((card_id % 100) == plati_unit_cards [x].getCardCode ())
						return plati_unit_cards [x];
				}
				break;
			case 24:	// Special Abilities.
				for (int x = 0; x < plati_s_a_cards.Length; x++) {
					if ((card_id % 100) == plati_s_a_cards [x].getCardCode ())
						return plati_s_a_cards [x];
				}
				break;
			case 25:	// Abilities.
				for (int x = 0; x < plati_spell_cards.Length; x++) {
					if ((card_id % 100) == plati_spell_cards [x].getCardCode ())
						return plati_spell_cards [x];
				}
				break;
			}
			break;
		}
		return null;
	}

	public bool isOverloaded(deck target_deck, int target_id){
		int count = 0;
		for (int u = 2; u < 20; u++) {
			int temp_card = target_deck.getCard (u);
			if (temp_card == 0)
				return false;
			if (temp_card == target_id) {
				count++;
				if (count == 2)
					return true;
			}
		}
		return false;
	}

	public Sprite getHumanFrame(){
		return frame_humans;
	}

	public Sprite getPlatiFrame(){
		return frame_plati;
	}

	public Sprite getHumanDeckFrame(){
		return deck_frames[0];
	}

	public Sprite getPlatiDeckFrame(){
		return deck_frames[1];
	}

	public Sprite getHumanBkg(){
		return bkg_humans;
	}

	public Sprite getPlatiBkg(){
		return bkg_plati;
	}

	public Sprite getSelectedHumans(){
		return selected_humans;
	}

	public Sprite getSelectedPlati(){
		return selected_plati;
	}

	public Sprite getHeroPortrait(int pos){
		return hero_portraits [pos];
	}

	public Sprite getBattleFrame(int race){
		if (race == 1)
			return battle_frame_humans;
		else
			return battle_frame_plati;
	}

	public Sprite getTurnCircle(int race, int state){
		//Debug.Log ("Trying to get turn circle: " + race + ", " + state);
		if (race == 1) {
			switch (state) {
			case 0:	// If queued.
				return bf_circles[5];
			case 1:	// If active now.
				return bf_circles[3];
			case 2:	// If disabled.
				return bf_circles[4];
			}
		} else {
			switch (state) {
			case 0:	// If queued.
				return bf_circles[2];
			case 1:	// If active now.
				return bf_circles[0];
			case 2:	// If disabled.
				return bf_circles[1];
			}
		}
		return null;
	}

	public void setSelectedSlot(int to_set){
		selected_slot = to_set;
	}

	public int getSelectedSlot(){
		return selected_slot;
	}

	public void setSelectedRace(int race){
		selected_race = race;
	}

	public int getSelectedRace(){
		return selected_race;
	}

	public void setSelectedHero(int hero){
		selected_hero = hero;
	}

	public int getSelectedHero(){
		return selected_hero;
	}

	public void setSelectedSAC(int sac){
		selected_sac = sac;
	}

	public int getSelectedSAC(){
		return selected_sac;
	}

	public void setNewDeckMode(bool input){
		new_deck_mode = input;
	}

	public bool getNewDeckMode(){
		return new_deck_mode;
	}

	public Sprite getCardBack(int race){
		if (race == 1)
			return card_back_humans;
		else
			return card_back_plati;
	}

	public deck getDeckByPos(int pos){
		return player_decks [pos];
	}

	public deck getDeckBySlot(int slot){
		/*for (int x = 0; x < player_decks.Length; x++) {
			Debug.Log ("Deck slot numbers: " + player_decks [x].getDeckSlot ());
			if (player_decks [x].getDeckSlot () == slot)
				return player_decks [x];
		}
		return null;*/
		//Debug.Log ("Calling deck slot number: " + slot);
		return player_decks [slot];
	}

	public card getHumanCard(int card_type, int card_code){
		switch(card_type){
		case 1:
			return human_hero_cards [card_code];
		case 2:
			return human_s_u_cards [card_code];
		case 3:
			return human_unit_cards [card_code];
		case 4:
			return human_s_a_cards [card_code];
		case 5:
			return human_spell_cards [card_code];
		}
		return null;
	}

	public card getPlatiCard(int card_type, int card_code){
		switch(card_type){
		case 1:
			return plati_hero_cards [card_code];
		case 2:
			return plati_s_u_cards [card_code];
		case 3:
			return plati_unit_cards [card_code];
		case 4:
			return plati_s_a_cards [card_code];
		case 5:
			return plati_spell_cards [card_code];
		}
		return null;
	}

	public Sprite getSprite(int pos, int category){
		switch (category) {
		case 0:	// Human Heroes.
			return human_heroes[pos];
		case 1:	// Plati Heroes.
			return plati_heroes[pos];
		case 2:	// Human Special Units.
			return human_special_units[pos];
		case 3:	// Plati Special Units.
			return plati_special_units[pos];
		case 4:	// Human Units.
			return human_units[pos];
		case 5:	// Plati Units.
			return plati_units[pos];
		case 6:	// Human Special Ability.
			return human_specials[pos];
		case 7:	// Plati Special Ability.
			return plati_specials[pos];
		case 8:	// Human Abilities.
			return human_spells[pos];
		case 9:	// Plati Abilities.
			return plati_spells[pos];
		case 11:	// Passive Effects Icons.
			return effect_icons [pos];
		}
		return null;
	}
		
}
