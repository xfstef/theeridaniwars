﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class deck {

	string deck_name = null;
	int[] the_cards = new int[20];	// The ids of all the cards in the deck.
	int race = 0;	// 1 for Humans, 2 for Plati.
	int deck_slot = 16;	// from 0 to 15.
	int cards_selected = 0;	// from 0 to 20.
	int hero_portrait;	// The hero portriat sprite ID.
	// TODO: Other attributes regarding Deck statistics: win rates, xp, etc.

	public void setDeckSlot(int ds){
		deck_slot = ds;
	}

	public int getDeckSlot(){
		return deck_slot;
	}

	public void initizalizeDeck(){
		for (int x = 0; x < 20; x++) {
			the_cards[x] = 0;
		}
	}

	public int getCard(int pos){
		return the_cards [pos];
	}

	public void setCard(int pos, int card_id){
		the_cards [pos] = card_id;
	}

	public void setName (string name){
		deck_name = name;
	}

	public string getName(){
		return deck_name;
	}

	public void setRace(int to_set){
		race = to_set;
	}

	public int getRace(){
		return race;
	}

	public void setAmountSelected(int amount){
		cards_selected = amount;
	}

	public int getAmountSelected(){
		return cards_selected;
	}

	public void setHeroPortrait(int temp){
		hero_portrait = temp;
	}

	public int getHeroPortrait(){
		return hero_portrait;
	}
}
