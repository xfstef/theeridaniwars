﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class newDeckAbility : MonoBehaviour {

	persistentObjects persistent_data;
	GameObject[] all_cards;
	GameObject bkg;
	Image bkg_image;
	card temp;
	card prefab;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("persistentGO");
		persistent_data = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
		bkg = GameObject.Find ("bkg");
		bkg_image = bkg.GetComponent<Image> ();

		if (persistent_data != null) {
			int selected_slot;
			int selected_race;
			int selected_hero;
			selected_slot = persistent_data.getSelectedSlot ();
			selected_race = persistent_data.getSelectedRace ();
			selected_hero = persistent_data.getSelectedHero ();

			Debug.Log ("The selected slot was: " + selected_slot + ", and the selected race was: " + selected_race + ", selec_hero: " + selected_hero);

			all_cards = GameObject.FindGameObjectsWithTag ("CardObject");
			Array.Sort (all_cards, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });

			prefab = gameObject.AddComponent<card> ();

			for (int x = 0; x < all_cards.Length; x++) {
				card temp;
				temp = all_cards [x].GetComponent<card> ();

				if (selected_race == 1) {
					switch(selected_hero){
					case 0:
						prefab = persistent_data.getHumanCard(4, x);
						break;
					case 1:
						prefab = persistent_data.getHumanCard(4, x);
						break;
					case 2:
						prefab = persistent_data.getHumanCard(4, x+3);
						break;
					case 3:
						prefab = persistent_data.getHumanCard(4, x+3);
						break;
					case 4:
						prefab = persistent_data.getHumanCard(4, x+6);
						break;
					case 5:
						prefab = persistent_data.getHumanCard(4, x+6);
						break;
					}
				} else {
					switch(selected_hero){
					case 0:
						prefab = persistent_data.getPlatiCard(4, x);
						break;
					case 1:
						prefab = persistent_data.getPlatiCard(4, x);
						break;
					case 2:
						prefab = persistent_data.getPlatiCard(4, x+3);
						break;
					case 3:
						prefab = persistent_data.getPlatiCard(4, x+3);
						break;
					case 4:
						prefab = persistent_data.getPlatiCard(4, x+6);
						break;
					case 5:
						prefab = persistent_data.getPlatiCard(4, x+6);
						break;
					}
				}
				temp.setName (prefab.getName ());
				temp.setRace (prefab.getRace ());
				temp.setCardType (prefab.getCardType ());
				temp.setCardGraphic (prefab.getCardGraphic ());
				temp.setCardCode (prefab.getCardCode ());
				temp.updateGraphics ();

				Debug.Log ("Card race: " + prefab.getRace ());
				temp.updateGraphics ();
			}

			if (selected_race == 1) {
				bkg_image.sprite = persistent_data.getHumanBkg ();
			} else {
				bkg_image.sprite = persistent_data.getPlatiBkg ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			if (persistent_data.getNewDeckMode())
				SceneManager.LoadScene ("newDeckHeroes");
			else
				SceneManager.LoadScene ("deckView");
	}

	public void BackPressed(){
		if (persistent_data.getNewDeckMode())
			SceneManager.LoadScene ("newDeckHeroes");
		else
			SceneManager.LoadScene ("deckView");
	}
}
