﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.EventSystems;

public class battlefield : MonoBehaviour {

	player player_save;
	deck the_deck, enemy_deck;
	Canvas menuCanv, initialSelCanv, battleCanvas, endGameCanv;
	GameObject battleMask, enemyBack, enemyBackAmount, playerDeck_cards, enemyDeck_cards, player_hand;
	Text playerCards_left, enemyCards_left, enemyBackAmount_text;
	GameObject[] initialOptions, playerCards, enemyCards, initialSelections, in_hand, enemy_hand, battlefield_selection_frames, battlefield_card_slots, all_battleCards;
	Image battleMaskImage, enemyBack_image, enemyBackAmount_image;
	persistentObjects decks_scripts;
	float screen_width_adjuster, screen_height_adjuster;
	int selected_race, selected_slot, amount_preselected, player_cards_left, enemy_cards_left, current_move, picked_up, player_cardsinhand, enemy_cardsinhand, current_active_card;
	int current_battlefield_age = 99;	// Used to mark each played unit when it gets played.
	int game_state = 0;	// This defines what is currently happening on the battlefield. 0 Means initial Unit Selection, 
						// 1 is for Unit Placement, 2 for Unit/Card Action, 3 for Hand Unit/Card Selection,
						// 4 for initial place 3 units, 5 for initial draw 3 cards.
	bool player_first;
	card current_turn;
	Dictionary<int, int> card_turns, pos_to_card;
	bool running = true;
	Sprite sel_verde, sel_rosu, sel_galben;
	skillLibrary skill_helper;

	// Use this for initialization
	void Start () {
		menuCanv = GameObject.Find ("CanvasMenu").GetComponent<Canvas> ();
		initialSelCanv = GameObject.Find ("CanvasInitial").GetComponent<Canvas> ();
		endGameCanv = GameObject.Find ("CanvasEndScreen").GetComponent<Canvas> ();
		menuCanv.enabled = false;
		initialSelCanv.enabled = true;
		endGameCanv.enabled = false;
		battleCanvas = GameObject.Find ("CanvasBattlefield").GetComponent<Canvas> ();
		GameObject.Find ("CanvasBattlefield").GetComponent<CanvasGroup> ().interactable = false;
		battleMask = GameObject.Find ("mask");
		battleMaskImage = battleMask.GetComponent<Image> ();
		battleMaskImage.enabled = true;
		amount_preselected = 0;
		all_battleCards = new GameObject [12];
		screen_width_adjuster = (float)(Screen.width / 1920.0);
		screen_height_adjuster = (float)(Screen.height / 1080.0);
		player_cardsinhand = 0;
		enemy_cardsinhand = 0;

		pos_to_card = new Dictionary<int, int> ();
		for (int x = 0; x < 12; x++) {
			int temp = x;
			pos_to_card.Add (temp, 99);
		}

		sel_verde = Resources.Load<Sprite> ("Menu/verde");
		sel_rosu = Resources.Load<Sprite> ("Menu/rosu");
		sel_galben = Resources.Load<Sprite> ("Menu/galben");

		battlefield_selection_frames = GameObject.FindGameObjectsWithTag ("bttfSelectionFrame");
		Array.Sort (battlefield_selection_frames, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });
		battlefield_card_slots = GameObject.FindGameObjectsWithTag ("bttfCardSlot");
		Array.Sort (battlefield_card_slots, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });

		for (int x = 0; x < 12; x++) {
			GameObject new_battle_card = Instantiate (Resources.Load ("Prefabs/battleCard"), transform.position, Quaternion.identity) as GameObject;
			all_battleCards [x] = new_battle_card;
		}

		playerDeck_cards = GameObject.Find ("playerCardsLeft");
		enemyDeck_cards = GameObject.Find ("enemyCardsLeft");
		playerCards_left = playerDeck_cards.GetComponent<Text> ();
		enemyCards_left = enemyDeck_cards.GetComponent<Text> ();
		player_hand = GameObject.Find ("playerCards");

		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;

		initialSelections = GameObject.FindGameObjectsWithTag ("selectionFrame");
		Array.Sort (initialSelections, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });

		if (decks_scripts != null) {
			generateEnemy ();

			enemyBack = GameObject.Find ("enemyCardBack");
			enemyBack_image = enemyBack.GetComponent<Image> ();
			enemyBackAmount = GameObject.Find ("enemyAmountBkg");
			enemyBackAmount_image = enemyBackAmount.GetComponent<Image> ();
			enemyBackAmount_text = enemyBackAmount.transform.Find ("amount").GetComponent<Text> ();

			if (enemy_deck.getRace () == 1) {
				enemyBack_image.sprite = decks_scripts.getCardBack (1);
				enemyBackAmount_image.sprite = decks_scripts.getSelectedHumans ();
			} else {
				enemyBack_image.sprite = decks_scripts.getCardBack (2);
				enemyBackAmount_image.sprite = decks_scripts.getSelectedPlati ();
			}

			selected_race = decks_scripts.getSelectedRace ();
			selected_slot = decks_scripts.getSelectedSlot ();
			player_save = decks_scripts.getPlayerSave ();
			the_deck = player_save.getDecks() [selected_slot];

			initialOptions = GameObject.FindGameObjectsWithTag ("CardObject");
			Array.Sort (initialOptions, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });
			pickFourUnits ();

			instantiateCards ();
			pickTwoEnemyCards ();

			playerCards_left.text = "Cards left: " + player_cards_left.ToString();
			enemyCards_left.text = "Enemy Cards left: " + enemy_cards_left.ToString();

			playerCards [0].GetComponent<card> ().setCardState (2);
			enemyCards [0].GetComponent<card> ().setCardState (2);

			current_move = UnityEngine.Random.Range (0, 2);	// Generating who goes first.
			if (current_move == 0)
				player_first = true;
			else
				player_first = false;
		}

		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate = 30;
	}
	
	// Update is called once per frame
	void Update () {
		//var pointer = new PointerEventData(EventSystem.current);
		if (Input.GetKeyDown (KeyCode.Escape))
			if (menuCanv.enabled == true)
				ResumeGame ();
			else
				MenuPressed ();

		if (!running) {
			running = true;
			parseGameState ();
		}

		for (int x = 0; x < 12; x++) {
			if (all_battleCards [x].GetComponent<card> ().getShaker () > 0) {
				all_battleCards [x].GetComponent<card> ().decrementShaker ();
				if (all_battleCards [x].GetComponent<card> ().getShaker () % 2 == 0)
					all_battleCards [x].transform.Translate (2*screen_width_adjuster,0,0);
				else
					all_battleCards [x].transform.Translate (-2*screen_width_adjuster,0,0);
			}
		}
	
	}

	public void MenuPressed(){
		initialSelCanv.GetComponent<CanvasGroup> ().interactable = false;
		battleCanvas.GetComponent<CanvasGroup> ().interactable = false;
		battleMaskImage.enabled = true;
		menuCanv.enabled = true;
	}

	public void ResumeGame(){
		if (amount_preselected < 2) {
			initialSelCanv.GetComponent<CanvasGroup> ().interactable = true;
			menuCanv.enabled = false;
		} else {
			battleCanvas.GetComponent<CanvasGroup> ().interactable = true;
			menuCanv.enabled = false;
		}
		battleMaskImage.enabled = false;
	}

	public void SurrenderPressed(){
		SceneManager.LoadScene ("deckScreen");
	}

	void updateBattleSlots (){
		for (int x = 0; x < 12; x++) {
			if (pos_to_card[x] < 12 && battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent () == true) {
				if (all_battleCards [pos_to_card[x]].GetComponent<card> ().getHealth () > 0) {
					battlefield_card_slots [x].SetActive (false);
					all_battleCards [pos_to_card[x]].transform.SetParent (battleCanvas.transform);
					all_battleCards [pos_to_card[x]].transform.localScale = new Vector3 (screen_width_adjuster, screen_height_adjuster, 0);
					all_battleCards [pos_to_card[x]].transform.localPosition = battlefield_card_slots [x].transform.localPosition;
				} else {
					if (all_battleCards [pos_to_card[x]].GetComponent<card> ().getCardType () == 1) {
						running = true;
						battleCanvas.GetComponent<CanvasGroup> ().interactable = false;
						battleMaskImage.enabled = true;
						if (all_battleCards [pos_to_card[x]].GetComponent<card> ().getIsEnemy () == true) {
							endGameCanv.transform.Find ("result").GetComponent<Text> ().text = "Victory!";
							endGameCanv.enabled = true;
						} else {
							endGameCanv.transform.Find ("result").GetComponent<Text> ().text = "Defeat.";
							endGameCanv.enabled = true;
						}
						return;
					}
					battlefield_card_slots [x].GetComponent<battleSlot> ().setCardPresent (false);
					battlefield_card_slots [x].SetActive(true);
					all_battleCards [pos_to_card[x]].transform.SetParent (null);
				}
			}
		}
	}

	void parseGameState (){	// Called after each move.

		//checkToPlace ();	// Checking if any Cards are pending placement on the battlefield. This is needed 
							// because at the beginning each player gets to play 3 cards total.

		Debug.Log("Running another loop! with game_state: " + game_state);
		switch (game_state) {
		case 1:	// This case for placing down card.
			preparePlaceDown();
			break;
		case 2:	// This case for using unit.
			activateNextCard();
			break;
		case 3:	// This case for Selecting a card from the hand.
			break;
		case 4:	// Placing all 6 initial cards mode.
			preparePlaceAllDown ();
			break;
		case 5:	// Drawing all initial cards into hand.
			drawAllCardsToHands ();
			initializeTurnTimers ();
			updateTurnMeters ();
			instantiateHandCards ();
			break;
		}
	}

	void cleanSlotSelections(){
		for (int x = 0; x < 12; x++){
			if (battlefield_selection_frames [x].GetComponent<Image> ().enabled == true)
				battlefield_card_slots [x].GetComponent<Button> ().onClick.RemoveAllListeners ();
			battlefield_selection_frames [x].GetComponent<Image> ().enabled = false;
			if(battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent ())
				all_battleCards [pos_to_card[x]].transform.Find("frame").GetComponent<Button> ().onClick.RemoveAllListeners ();
		}
	}

	void activateNextCard(){
		for (int x = 0; x < 12; x++) {
			int temp = x;
			if (all_battleCards [x].GetComponent<card> ().getTurnTimer () == 0) {
				current_active_card = temp;

				battlefield_selection_frames [x].GetComponent<Image> ().sprite = sel_verde;
				battlefield_selection_frames [x].GetComponent<Image> ().enabled = true;

				all_battleCards [x].transform.Find("frame").GetComponent<Button> ().onClick.AddListener (delegate() {
					skipTurn (current_active_card);
				});

				if (all_battleCards [x].GetComponent<card> ().getIsEnemy () == true) {
					highlightMoveOptions (x, false);
					Invoke ("runAI", 1);
				} else {
					highlightMoveOptions (x, true);
					addListeners ();
				}
				return;
			}
		}

		// Reached end of Round!
		drawOnNewRound();
		instantiateHandCards ();
		initializeTurnTimers ();
		updateTurnMeters ();
		running = false;
	}

	void drawOnNewRound(){
		int random_number = UnityEngine.Random.Range (2, 20);

		if (player_cardsinhand < 5 && player_cards_left > 0) {
			while(playerCards[random_number].GetComponent<card>().getCardState() != 0) {
				random_number = UnityEngine.Random.Range (2, 20);
			}
			playerCards [random_number].GetComponent<card> ().setCardState (1);
			player_cardsinhand++;
			player_cards_left--;
		}
	}

	void skipTurn(int card){
		if (card >= 0)
			cleanSlotSelections ();	// And click listeners.
		else
			return;

		updateTurnMeters ();
		running = false;
	}

	void addListeners(){
		for (int x = 0; x < 12; x++) {
			int temp = x;
			if (battlefield_selection_frames [x].GetComponent<Image> ().enabled == true) {
				if (battlefield_selection_frames [x].GetComponent<Image> ().sprite == sel_verde) {
					battlefield_card_slots [x].GetComponent<Button> ().onClick.AddListener (delegate() {
						moveTo (temp);
					});
				}
				if (battlefield_selection_frames [x].GetComponent<Image> ().sprite == sel_galben) {
					all_battleCards [pos_to_card[x]].transform.Find("frame").GetComponent<Button> ().onClick.AddListener (delegate() {
						attackRanged (temp);
					});
				}
				if (battlefield_selection_frames [x].GetComponent<Image> ().sprite == sel_rosu) {
					all_battleCards [pos_to_card[x]].transform.Find("frame").GetComponent<Button> ().onClick.AddListener (delegate() {
						attackMelee (temp);
					});
				}
			}
		}
	}

	void moveTo(int where){
		Debug.Log ("Trying to move " + current_active_card + ", to position " + where);

		Debug.Log ("0:" + pos_to_card [0] + ", 1:" + pos_to_card[1] + ", 2:" + pos_to_card[2] + ", 3:" + pos_to_card[3] + ", 4:" + pos_to_card[4] + ", 5:" + pos_to_card[5]
			+ ", 6:" + pos_to_card[6] + ", 7:" + pos_to_card[7] + ", 8:" + pos_to_card[8] + ", 9:" + pos_to_card[9] + ", 10:" + pos_to_card[10] + ", 11:" + pos_to_card[11]);


		cleanSlotSelections ();

		//all_battleCards[where].GetComponent<card> ().setCardID (all_battleCards [current_active_card].GetComponent<card> ().getCardID ());
		//all_battleCards[where].GetComponent<card> ().appropriateCard ();
		//if (all_battleCards [current_active_card].GetComponent<card> ().getIsEnemy () == true)
		//	all_battleCards [where].GetComponent<card> ().setIsEnemy ();
		//else
		//	all_battleCards [where].GetComponent<card> ().setNotEnemey ();
		//all_battleCards [where].GetComponent<card> ().setBattleState (2);
		//all_battleCards [where].GetComponent<card> ().updateBattleGraphics ();
		battlefield_card_slots [where].GetComponent<battleSlot> ().setCardPresent (true);

		for (int x = 0; x < 12; x++) {
			if (pos_to_card [x] == current_active_card) {
				pos_to_card [x] = 99;
				battlefield_card_slots [x].GetComponent<battleSlot> ().setCardPresent (false);
				battlefield_card_slots [x].SetActive(true);
				break;
			}
		}
		pos_to_card [where] = current_active_card;
		//all_battleCards [current_active_card].transform.SetParent (null);

		updateBattleSlots ();
		updateTurnMeters ();
		running = false;

		Debug.Log ("0:" + pos_to_card [0] + ", 1:" + pos_to_card[1] + ", 2:" + pos_to_card[2] + ", 3:" + pos_to_card[3] + ", 4:" + pos_to_card[4] + ", 5:" + pos_to_card[5]
			+ ", 6:" + pos_to_card[6] + ", 7:" + pos_to_card[7] + ", 8:" + pos_to_card[8] + ", 9:" + pos_to_card[9] + ", 10:" + pos_to_card[10] + ", 11:" + pos_to_card[11]);
	}

	void attackRanged(int who){
		Debug.Log (current_active_card + " is trying to shoot " + who);
		cleanSlotSelections ();

		all_battleCards [pos_to_card[who]].GetComponent<card> ().cardHit (all_battleCards [current_active_card].GetComponent<card> ().getDamage (), false);
		if (all_battleCards [pos_to_card[who]].GetComponent<card> ().getHealth () <= 0) {	// Attacked Card died.
			all_battleCards [pos_to_card[who]].GetComponent<card> ().setShaker (30);
			Invoke ("waitingForAnimations", 1);
		} else {
			all_battleCards [pos_to_card[who]].GetComponent<card> ().setShaker (10);
			Invoke ("waitingForAnimations", 0.5f);
		}
	}

	void attackMelee(int who){
		Debug.Log (current_active_card + " is trying to attack " + who);
		cleanSlotSelections ();
		bool long_wait = false;

		int retaliation = all_battleCards [pos_to_card[who]].GetComponent<card> ().cardHit (all_battleCards [current_active_card].GetComponent<card> ().getDamage (), true);
		bool still_alive = all_battleCards [current_active_card].GetComponent<card> ().retaliatedAgainst (retaliation);

		if (all_battleCards [pos_to_card[who]].GetComponent<card> ().getHealth () <= 0) {	// Attacked Card died.
			all_battleCards [pos_to_card[who]].GetComponent<card> ().setShaker(30);
			long_wait = true;
		} else {
			all_battleCards [pos_to_card[who]].GetComponent<card> ().setShaker (10);
		}

		if (all_battleCards [current_active_card].GetComponent<card> ().getHealth () <= 0) {	// Attacker Card died.
			all_battleCards [current_active_card].GetComponent<card> ().setShaker(30);
			long_wait = true;
		} else {
			all_battleCards [current_active_card].GetComponent<card> ().setShaker (10);
		}

		if(long_wait == true)
			Invoke ("waitingForAnimations", 1);
		else
			Invoke ("waitingForAnimations", 0.5f);
	}

	void waitingForAnimations(){
		updateBattleSlots ();
		updateTurnMeters ();
		running = false;
	}

	void highlightMoveOptions(int pos, bool player_turn){
		card the_card = all_battleCards [pos].GetComponent<card> ();
		int card_pos = -1;
		for (int x = 0; x < 12; x++) {
			if (pos_to_card [x] == pos) {
				card_pos = x;
				break;
			}
		}
		Vector2 temp_pos = battlefield_card_slots [card_pos].GetComponent<battleSlot> ().getXY ();
		//Debug.Log ("The id: " + pos + ", at pos: " + temp_pos);
		battleSlot temp_slot;
		int temp;

		if (temp_pos.x - 1.0 >= 0.0) {	// Checking to the Left.
			if (temp_pos.y - 1.0 >= 0.0) {	// Checking Upper Left Corner.
				temp_slot = findBattleSlotAt(temp_pos.x - 1.0, temp_pos.y - 1.0);
				temp = temp_slot.getID ();
				if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
					if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						//Aici eram, cautam prin battlefield_selection_frames unde trebuie sa modific iteratorul cu pos_to_card[x]
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}
				} else {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} 
			}

			// Checking to the Left.
			temp_slot = findBattleSlotAt(temp_pos.x - 1.0, temp_pos.y);
			temp = temp_slot.getID ();
			if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
				if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				}
			} else {
				battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
				battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
			}

			if (temp_pos.y + 1.0 < 3.0) {	// Checking Lower Left Corner.
				temp_slot = findBattleSlotAt(temp_pos.x - 1.0, temp_pos.y + 1.0);
				temp = temp_slot.getID ();
				if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
					if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}
				} else {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} 
			}
		}

		if (temp_pos.x + 1.0 < 4.0) {	// Checking to the Right.
			if (temp_pos.y - 1.0 >= 0.0) {	// Checking Upper Right Corner.
				temp_slot = findBattleSlotAt(temp_pos.x + 1.0, temp_pos.y - 1.0);
				temp = temp_slot.getID ();
				if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
					if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}
				} else {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} 
			}

			// Checking to the Right.
			temp_slot = findBattleSlotAt(temp_pos.x + 1.0, temp_pos.y);
			temp = temp_slot.getID ();
			if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
				if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				}
			} else {
				battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
				battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
			}

			if (temp_pos.y + 1.0 < 3.0) {	// Checking Lower Right Corner.
				temp_slot = findBattleSlotAt(temp_pos.x + 1.0, temp_pos.y + 1.0);
				temp = temp_slot.getID ();
				if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
					if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}
				} else {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} 
			}
		}

		if (temp_pos.y - 1.0 >= 0.0) {	// Checking Up.
			temp_slot = findBattleSlotAt(temp_pos.x, temp_pos.y - 1.0);
			temp = temp_slot.getID ();
			if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
				if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				}
			} else {
				battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
				battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
			} 
		}

		if (temp_pos.y + 1.0 < 3.0) {	// Checking Down.
			temp_slot = findBattleSlotAt(temp_pos.x, temp_pos.y + 1.0);
			temp = temp_slot.getID ();
			if ((the_card.getDamage() > 0) && temp_slot.getCardPresent () == true) {
				if ((all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				} else if (!(all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
					battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_rosu;
					battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
				}
			} else {
				battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_verde;
				battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
			} 
		}

		if(the_card.getDamage() > 0) {	// Adding ranged attacker selection frames if the card has attack damage.
			switch (the_card.getRange ()) {
			case 1:	// 1 Range
				if (temp_pos.x - 2.0 >= 0.0) {	// Checking to the far Left.
					if (temp_pos.y - 2.0 >= 0.0) {	// Checking far Upper far Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 2.0, temp_pos.y - 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.y - 1.0 >= 0.0) {	// Checking Upper far Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 2.0, temp_pos.y - 1.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					// Checking to the far Left.
					temp_slot = findBattleSlotAt (temp_pos.x - 2.0, temp_pos.y);
					temp = temp_slot.getID ();
					if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}

					if (temp_pos.y + 1.0 < 3.0) {	// Checking Lower far Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 2.0, temp_pos.y + 1.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.y + 2.0 < 3.0) {	// Checking far Lower far Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 2.0, temp_pos.y + 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}
				}

				if (temp_pos.x + 2.0 < 4.0) {	// Checking to the far Right.
					if (temp_pos.y - 2.0 >= 0.0) {	// Checking far Upper far Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 2.0, temp_pos.y - 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.y - 1.0 >= 0.0) {	// Checking Upper far Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 2.0, temp_pos.y - 1.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					// Checking to the far Right.
					temp_slot = findBattleSlotAt (temp_pos.x + 2.0, temp_pos.y);
					temp = temp_slot.getID ();
					if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}

					if (temp_pos.y + 2.0 < 3.0) {	// Checking far Lower far Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 2.0, temp_pos.y + 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.y + 1.0 < 3.0) {	// Checking Lower far Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 2.0, temp_pos.y + 1.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}
				}

				if (temp_pos.y - 2.0 >= 0.0) {	// Checking far Up.
					temp_slot = findBattleSlotAt (temp_pos.x, temp_pos.y - 2.0);
					temp = temp_slot.getID ();
					if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}

					if (temp_pos.x - 1.0 >= 0.0) {	// Checking far Upper Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 1.0, temp_pos.y - 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.x + 1.0 < 4.0) {	// Checking far Upper Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 1.0, temp_pos.y - 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}
				}

				if (temp_pos.y + 2.0 < 3.0) {	// Checking far Down.
					temp_slot = findBattleSlotAt (temp_pos.x, temp_pos.y + 2.0);
					temp = temp_slot.getID ();
					if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
						battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
					}

					if (temp_pos.x - 1.0 >= 0.0) {	// Checking far Lower Left Corner.
						temp_slot = findBattleSlotAt (temp_pos.x - 1.0, temp_pos.y + 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}

					if (temp_pos.x + 1.0 < 4.0) {	// Checking far Lower Right Corner.
						temp_slot = findBattleSlotAt (temp_pos.x + 1.0, temp_pos.y + 2.0);
						temp = temp_slot.getID ();
						if (temp_slot.getCardPresent () == true && (all_battleCards [pos_to_card[temp]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
							battlefield_selection_frames [temp].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [temp].GetComponent<Image> ().enabled = true;
						}
					}
				}
				break;
			case 2:	// 2 Range
				for (int x = 0; x < 12; x++) {
					if (battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent () == true &&
						(all_battleCards [pos_to_card[x]].GetComponent<card> ().getIsEnemy () & player_turn) == true) {
						if (!battlefield_selection_frames [x].GetComponent<Image> ().enabled) {
							battlefield_selection_frames [x].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [x].GetComponent<Image> ().enabled = true;
						}
					} else if (battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent () == true &&
						!(all_battleCards [pos_to_card[x]].GetComponent<card> ().getIsEnemy ()) && !player_turn) {
						if (!battlefield_selection_frames [x].GetComponent<Image> ().enabled) {
							battlefield_selection_frames [x].GetComponent<Image> ().sprite = sel_galben;
							battlefield_selection_frames [x].GetComponent<Image> ().enabled = true;
						}
					}
				}
				break;
			}
		}
		//Debug.Log ("The id: " + pos + ", at pos: " + temp_pos);
	}

	battleSlot findBattleSlotAt(double x, double y){
		for(int z = 0; z < 12; z++){
			Vector2 temp_v2 = battlefield_card_slots[z].GetComponent<battleSlot>().getXY();
			if(temp_v2.x == x && temp_v2.y == y)
				return battlefield_card_slots[z].GetComponent<battleSlot>();
		}
		return null;
	}

	void instantiateHandCards () {
		float start_y, step;
		Vector2 start_pos = new Vector2(0,0);

		switch (player_cardsinhand) {	// Establish y position step.
		case 1:
			start_y = 0;
			step = 0;
			start_pos.y = (start_y);
			break;
		case 2:
			start_y = -56 * screen_height_adjuster;
			step = 112 * screen_height_adjuster;
			start_pos.y = (start_y);
			break;
		case 3:
			start_y = -111 * screen_height_adjuster;
			step = 111 * screen_height_adjuster;
			start_pos.y = (start_y);
			break;
		case 4:
			start_y = -167 * screen_height_adjuster;
			step = 111 * screen_height_adjuster;
			start_pos.y = (start_y);
			break;
		case 5:
			start_y = -224 * screen_height_adjuster;
			step = 112 * screen_height_adjuster;
			start_pos.y = (start_y);
			break;
		default:
			start_y = 0;
			step = 0;
			start_pos.y = (start_y);
			break;
		}

		for (int x = 1; x < 20; x++)
			if (playerCards [x].GetComponent<card> ().getCardState () == 1) {
				playerCards [x].transform.SetParent (GameObject.Find ("CanvasBattlefield/playerCards").transform);
				playerCards [x].transform.localScale = new Vector3 (screen_width_adjuster, screen_height_adjuster, 0);
				playerCards [x].transform.transform.localPosition = start_pos;
				start_pos.y += step;
			}

		game_state = 2;
		running = false;
	}

	void drawAllCardsToHands(){
		for (int x = 0; x < 12; x++) {
			battlefield_selection_frames [x].GetComponent<Image> ().enabled = false;
			battlefield_card_slots[x].transform.GetComponent<Button> ().onClick.RemoveAllListeners ();
		}

		int random_number = 0;
		while (player_cardsinhand < 3) {
			random_number = UnityEngine.Random.Range (2, 20);
			if (playerCards [random_number].GetComponent<card> ().getCardState() == 0) {
				playerCards [random_number].GetComponent<card> ().setCardState (1);
				player_cardsinhand++;
			}
		}

		for (int x = 2; x < 20; x++) {	// Drawing cards to enemy hand. (Single Player Only)
			if(enemy_cardsinhand < 3){
				if (enemyCards [x].GetComponent<card> ().getCardState() == 0) {
					enemyCards [x].GetComponent<card> ().setCardState (1);
					enemy_cardsinhand++;
				}
			}else
				break;
		}

		if (playerCards [1].GetComponent<card> ().getIsPassive ())
			playerCards [1].GetComponent<card> ().setCardState (3);
		else {
			playerCards [1].GetComponent<card> ().setCardState (1);
			player_cardsinhand++;
		}
		player_cards_left = 13;
		playerDeck_cards.GetComponent<Text> ().text = "Cards left " + player_cards_left.ToString (); 

		if (enemyCards [1].GetComponent<card> ().getIsPassive ())
			enemyCards [1].GetComponent<card> ().setCardState (3);
		else {
			enemyCards [1].GetComponent<card> ().setCardState (1);
			enemy_cardsinhand++;
		}
		enemy_cards_left = 13;
		enemyDeck_cards.GetComponent<Text> ().text = "Enemy Cards left " + enemy_cards_left.ToString (); 
		enemyBackAmount_text.text = enemy_cardsinhand.ToString ();

	}

	void initializeTurnTimers(){	// Used to calculate the turns for every card at the start of a round.
		card_turns = new Dictionary<int, int>();
		for (int x = 0; x < all_battleCards.Length; x++) {
			if(pos_to_card[x] < 12 && all_battleCards [pos_to_card[x]].GetComponent<card> ().getInitiative () != 0 && 
				battlefield_card_slots[x].GetComponent<battleSlot>().getCardPresent() == true)
				card_turns.Add (x, all_battleCards [pos_to_card[x]].GetComponent<card> ().getInitiative ());
		}
		int turn = card_turns.Count;
		var list = card_turns.Keys.ToList ();
		list.Sort ();
		foreach (var card in card_turns.OrderBy(i => i.Value)) {
			//Debug.Log ("Initiative: " + card);
			all_battleCards [pos_to_card[card.Key]].GetComponent<card> ().setTurnTimer (turn);
			turn--;
			all_battleCards [pos_to_card[card.Key]].GetComponent<card> ().setBattleState (0);
			all_battleCards [pos_to_card[card.Key]].GetComponent<card> ().updateTurnMeter ();
		}
	}

	void updateTurnMeters(){	// Used to decrement turn meters for Units.
		for (int x = 0; x < all_battleCards.Length; x++)
			if(all_battleCards[x].GetComponent<card>().getInitiative () != 0) {
				all_battleCards [x].GetComponent<card> ().decrementTurnTimer (1);
				all_battleCards [x].GetComponent<card> ().updateTurnMeter ();
		}
	}

	void preparePlaceDown(){	// Place down one card from one specific player.

	}

	void preparePlaceAllDown(){	// We place all cards in hand down. For both players.
		checkToPlace ();
		if (game_state == 5) {
			running = false;
			return;
		}

		//Debug.Log("Running another loop! with current_move: " + current_move);
		if (current_move == 0) {	// If it's the player's move.
			current_move++;
			for (int x = 0; x < 6; x++) {
				battlefield_selection_frames [x + 6].GetComponent<Image> ().enabled = false;
				//battlefield_card_slots[x + 6].transform.GetComponent<Button> ().onClick.RemoveAllListeners ();
				int new_int = x;
				if (battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent () != true) {
					battlefield_selection_frames [x].GetComponent<Image> ().enabled = true;
					battlefield_card_slots[x].transform.GetComponent<Button> ().onClick.RemoveAllListeners ();
					battlefield_card_slots[x].transform.GetComponent<Button> ().onClick.AddListener(delegate() {
						prepareBattleCard(true, new_int);
					});
				}
			}

		} else {	// If it's the enemy's move.
			current_move--;
			for (int x = 6; x < 12; x++) {
				battlefield_selection_frames [x - 6].GetComponent<Image> ().enabled = false;
				battlefield_card_slots[x - 6].transform.GetComponent<Button> ().onClick.RemoveAllListeners ();
				int new_int = x;
				if (battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent () != true){
					battlefield_selection_frames [x].GetComponent<Image> ().enabled = true;
					//battlefield_card_slots[x].transform.GetComponent<Button> ().onClick.RemoveAllListeners ();
					//battlefield_card_slots[x].transform.GetComponent<Button>().onClick.AddListener(delegate() {
					//	prepareBattleCard(false, new_int);
					//});
				}
			}
			//Debug.Log ("Before");
			//StartCoroutine(waitForAI(1f));
			Invoke("runAI", 1);
			//runAI ();
		}
	}

	void runAI(){
		int optimal_spot;
		//Debug.Log ("Called AI on game_state: " + game_state);
		switch (game_state) {
		case 1:	// Placing down card mode.
			optimal_spot = calculateOptimalSpot ();
			//var pointer = new PointerEventData(EventSystem.current);
			//ExecuteEvents.Execute(battlefield_card_slots[optimal_spot].transform.GetComponent<Button> ().gameObject, pointer, ExecuteEvents.submitHandler);
			prepareBattleCard(false, optimal_spot);
			break;
		case 2:	// Moving card.
			skipTurn(current_active_card);
			break;
		case 3:
			optimal_spot = 0;
			break;
		case 4:
			optimal_spot = calculateOptimalSpot ();
			prepareBattleCard(false, optimal_spot);
			break;
		case 5:
			break;
		default:
			optimal_spot = 0;
			break;
		}
	}

	//IEnumerator waitForAI(float the_time){
		//yield WaitForSeconds(the_time);
		//runAI ();
	//}

	void prepareBattleCard (bool who, int position){	// Called on slot clicked.
		//Debug.Log ("Position: " + position + ", who: " + who);
		int switch_pos = position;
		if (pos_to_card [position] != 99)
			for (int x = 0; x < 12; x++) {
				if (pos_to_card [x] == 99) {
					pos_to_card [x] = position;
					switch_pos = x;
				}
			}
		else 
			pos_to_card [position] = position;

		if (who == true) {	// Needed for the player cards.
			//temp = playerCards [picked_up].GetComponent<card> ();
			playerCards [picked_up].GetComponent<card> ().setPlacedDown (true);
			all_battleCards[switch_pos].GetComponent<card> ().setCardID (playerCards [picked_up].GetComponent<card> ().getCardID ());
			all_battleCards[switch_pos].GetComponent<card> ().appropriateCard ();
			all_battleCards [switch_pos].GetComponent<card> ().appropriateInitiative (current_battlefield_age);
			current_battlefield_age--;
			all_battleCards [switch_pos].GetComponent<card> ().setBattleState (2);
			all_battleCards [switch_pos].GetComponent<card> ().updateBattleGraphics ();
			battlefield_card_slots [position].GetComponent<battleSlot> ().setCardPresent (true);
			playerCards [picked_up].GetComponent<card> ().setCardState (3);
			if (all_battleCards [switch_pos].GetComponent<card> ().getCardType () == 1 && playerCards[1].GetComponent<card>().getIsPassive() == true) {
				activateSkill (playerCards [1].GetComponent<card> ().getSkill(), all_battleCards[switch_pos].GetComponent<card> ());
			}
		} else {	// Needed for the enemy cards.
			//temp = enemyCards [picked_up].GetComponent<card> ();
			enemyCards [picked_up].GetComponent<card> ().setPlacedDown (true);
			all_battleCards[switch_pos].GetComponent<card> ().setCardID (enemyCards [picked_up].GetComponent<card> ().getCardID ());
			all_battleCards[switch_pos].GetComponent<card> ().appropriateCard ();
			all_battleCards [switch_pos].GetComponent<card> ().appropriateInitiative (current_battlefield_age);
			current_battlefield_age--;
			all_battleCards [switch_pos].GetComponent<card> ().setBattleState (2);
			all_battleCards [switch_pos].GetComponent<card> ().updateBattleGraphics ();
			all_battleCards [switch_pos].GetComponent<card> ().setIsEnemy ();
			battlefield_card_slots [position].GetComponent<battleSlot> ().setCardPresent (true);
			enemyCards [picked_up].GetComponent<card> ().setCardState (3);
			if (all_battleCards [switch_pos].GetComponent<card> ().getCardType () == 1 && enemyCards[1].GetComponent<card>().getIsPassive() == true) {
				activateSkill (enemyCards [1].GetComponent<card> ().getSkill(), all_battleCards[switch_pos].GetComponent<card> ());
			}
		}

		updateBattleSlots ();
		//Debug.Log ("Is placed: " + all_battleCards[position].GetComponent<card> ().getCardID () + " with age: " + all_battleCards[position].GetComponent<card> ().getBattlefieldAge());
		//parseGameState ();
		running = false;
	}

	void activateSkill (skill the_skill, card target){
		if (the_skill.getPassive () == true) {
			target.addEffectIcon (the_skill);
		} else {
			// TODO: Run special effect / animation.
		}
	}

	int calculateOptimalSpot(){
		card temp = enemyCards[picked_up].GetComponent<card> ();

		switch (temp.getCardType()) {
		case 1:
			return 9;
		case 2:
			if (temp.getRange () == 0) {
				for (int x = 6; x < 9; x++) {
					int temp_value = x;
					if (!battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent ())
						return temp_value;
				}
			} else {
				for (int x = 10; x < 12; x++) {
					int temp_value = x;
					if (!battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent ())
						return temp_value;
				}
			}
			break;
		case 3:
			if (temp.getRange () == 0) {
				for (int x = 6; x < 9; x++) {
					int temp_value = x;
					if (!battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent ())
						return temp_value;
				}
			} else {
				for (int x = 10; x < 12; x++) {
					int temp_value = x;
					if (!battlefield_card_slots [x].GetComponent<battleSlot> ().getCardPresent ())
						return temp_value;
				}
			}
			break;
		}
			
		return 0;
	}

	void checkToPlace(){
		
		if(current_move == 0){
			for (int x = 0; x < 20; x++) {
				//Debug.Log ("Card State: " + playerCards[x].GetComponent<card>().getCardState());
				if (playerCards [x].GetComponent<card> ().getCardState() == 2) {
					//Debug.Log ("Adding card to picked up: " + playerCards[x].GetComponent<card>().getCardID());
					picked_up = x;
					game_state = 4;
					return;
				}
			}
		} else {
			for (int x = 0; x < 20; x++) {
				//Debug.Log ("Card State: " + enemyCards[x].GetComponent<card>().getCardState());
				if (enemyCards [x].GetComponent<card> ().getCardState() == 2) {
					//Debug.Log ("Adding enemy card to picked up: " + enemyCards[x].GetComponent<card>().getCardID());
					picked_up = x;
					game_state = 4;
					return;
				}
			}
		}
		//Debug.Log ("Finished adding all initial cards!");
		picked_up = 99;
		game_state = 5;
	}

	void pickFourUnits () {
		int random_number, temp;
		int[] banned_numbers = new int[4];

		for (int x = 0; x < 4; x++) {
			random_number = UnityEngine.Random.Range (2, 20);
			for (int y = 0; y < banned_numbers.Length; y++)
				if (banned_numbers [y] == random_number) {
					random_number = UnityEngine.Random.Range (2, 20);
					y = 0;
				}
			//temp = playerCards [random_number].GetComponent<card> ().getCardID ();
			temp = the_deck.getCard (random_number);
			if ((temp / 100) % 10 == 5) {
				x--;
			} else {
				//card temp_card = initialOptions [x].GetComponent<card> ();
				initialOptions [x].GetComponent<card> ().setCardID (temp);
				initialOptions [x].GetComponent<card> ().appropriateCard ();
				initialOptions [x].GetComponent<card> ().updateGraphics ();
				banned_numbers [x] = random_number;
			}
		}
	}

	void pickTwoEnemyCards(){
		int to_be_played = 0;
		int random_to_be_played;

		while (to_be_played < 2) {
			random_to_be_played = UnityEngine.Random.Range (0, 30);
			if (random_to_be_played < 10) {
				for (int x = 2; x < 20; x++){
					if (enemyCards [x].GetComponent<card> ().getCardType () == 2 && enemyCards [x].GetComponent<card> ().getCardState () == 0) {
						enemyCards [x].GetComponent<card> ().setCardState (2);
						to_be_played++;
						//Debug.Log ("Selected special unit ID: " + enemyCards [x].GetComponent<card> ().getCardID());
						break;
					}
				}
			} else {
				for (int x = 2; x < 20; x++){
					if (enemyCards [x].GetComponent<card> ().getCardType () == 3 && enemyCards [x].GetComponent<card> ().getCardState () == 0) {
						enemyCards [x].GetComponent<card> ().setCardState (2);
						//Debug.Log ("Selected unit ID: " + enemyCards [x].GetComponent<card> ().getCardID());
						to_be_played++;
						break;
					}
				}
			}
		}
	}

	void instantiateCards(){
		playerCards = new GameObject[20];
		enemyCards = new GameObject[20];
		for (int x = 0; x < 20; x++) {
			playerCards[x] = Instantiate (Resources.Load("Prefabs/HeroCard"), transform.position, Quaternion.identity) as GameObject;
			enemyCards[x] = Instantiate (Resources.Load("Prefabs/HeroCard"), transform.position, Quaternion.identity) as GameObject;

			playerCards [x].GetComponent<card> ().setCardID (the_deck.getCard (x));
			enemyCards [x].GetComponent<card> ().setCardID (enemy_deck.getCard (x));

			playerCards[x].GetComponent<card>().appropriateCard();
			enemyCards[x].GetComponent<card>().appropriateCard();

			playerCards [x].GetComponent<card> ().updateGraphics ();
			enemyCards [x].GetComponent<card> ().updateGraphics ();
		}
	}

	void generateEnemy () {
		enemy_deck = new deck ();
		int random_number, temp_id, race;

		random_number = UnityEngine.Random.Range (1,3);
		race = random_number;
		enemy_deck.setRace (race);
		random_number = UnityEngine.Random.Range (0, 6);
		enemy_deck.setCard (0, (race * 1000 + 100 + random_number));
		//Debug.Log ("Enemy Hero: " + enemy_deck.getCard(0));
		random_number = UnityEngine.Random.Range (0, 3);
		enemy_deck.setCard (1, (race * 1000 + 400 + random_number + (3 * decks_scripts.retrieveCard (enemy_deck.getCard (0)).getHeroClassOffset ())));
		//Debug.Log ("Enemy SAC: " + enemy_deck.getCard(1));

		for (int x = 2; x < 20; x++) {
			random_number = UnityEngine.Random.Range (2, 5);
				
			switch (random_number) {
			case 2:	// Adding special unit.
				random_number = UnityEngine.Random.Range (0, 6);
				temp_id = race * 1000 + 200 + random_number;
				if (decks_scripts.isOverloaded (enemy_deck, temp_id)) {
					x--;
					break;
				}
				enemy_deck.setCard (x, temp_id);
				//Debug.Log ("New Enemy: " + enemy_deck.getCard(x));
				break;
			case 3:	// Adding normal unit.
				random_number = UnityEngine.Random.Range (0, 10);
				temp_id = race * 1000 + 300 + random_number;
				if (decks_scripts.isOverloaded(enemy_deck, temp_id)) {
					x--;
					break;
				}
				enemy_deck.setCard (x, temp_id);
				//Debug.Log ("New Enemy: " + enemy_deck.getCard(x));
				break;
			case 4:	// Adding spell.
				random_number = UnityEngine.Random.Range (0, 5);
				temp_id = race * 1000 + 500 + random_number;
				if (decks_scripts.isOverloaded(enemy_deck, temp_id)) {
					x--;
					break;
				}
				enemy_deck.setCard (x, temp_id);
				//Debug.Log ("New Enemy: " + enemy_deck.getCard(x));
				break;
			}
		}
	}

	public void selectInitial (int pos) {
		//Debug.Log ("Selected: " + pos);
		if (initialSelections [pos].GetComponent<Image> ().enabled == true) {
			initialSelections [pos].GetComponent<Image> ().enabled = false;
			amount_preselected--;
		} else {
			initialSelections [pos].GetComponent<Image> ().enabled = true;
			for (int x = 2; x < 20; x++) {

			}
			amount_preselected++;
		}
		if (amount_preselected == 2) {
			for (int x = 0; x < 4; x++) {
				if (initialSelections [x].GetComponent<Image> ().enabled == true) {
					for(int y = 2; y < 20; y++){
						if (initialOptions [x].GetComponent<card> ().getCardID () == playerCards [y].GetComponent<card> ().getCardID ()
								&& playerCards [y].GetComponent<card> ().getCardState() != 2) {
							playerCards [y].GetComponent<card> ().setCardState (2);	
							break;
						}
					}
				}
			}

			initialSelCanv.enabled = false;
			GameObject.Find ("CanvasBattlefield").GetComponent<CanvasGroup> ().interactable = true;
			battleMaskImage.enabled = false;

			game_state = 4;
			//parseGameState ();
			running = false;
		}
	}

	public void openFacebook(){
		Application.OpenURL("https://www.facebook.com/theeridaniwars/");
	}

	public void openPatreon(){
		Application.OpenURL("https://www.patreon.com/theeridaniwars");
	}

}
