﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class mainMenuScrips : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			Application.Quit ();
	}

	public void LoadDecks()  {
		//UnityEngine.SceneManagement.SceneManager.LoadScene ("deckScreen");
		SceneManager.LoadScene ("deckScreen");
	}

	public void LoadOptions()  {
		//UnityEngine.SceneManagement.SceneManager.LoadScene ("optionsScreen");
		SceneManager.LoadScene ("optionsScreen");
	}

	public void ExitGame()  {
		Application.Quit ();
	}
}
