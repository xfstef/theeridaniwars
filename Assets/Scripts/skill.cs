﻿using UnityEngine;
using System.Collections;

public class skill : MonoBehaviour {

	int skillID;	// 1xx for special hero skills,
	// 2xx for other spells and items.
	string skillName;
	bool singleTarget = true;
	bool passive = false;
	bool recurring_effect = false;
	int trigger_id = 0;	// Only needed for skills that have triggers.
						// 1x - Aimed at hero.
						// 2x - Direct heal.
						// 3x - Periodic heal.
						// 4x - Extra damage.
						// 5x - Summoning.
						// 6x - Unit invicibilities.
						// 7x - Special unit effects.
	int effect_icon = -1;	// -1 - No icon needed. 0+ icon sprite sheet ID.

	public void setSkillID(int temp){
		skillID = temp;
	}

	public int getSkillID(){
		return skillID;
	}

	public void setSkillName(string temp){
		skillName = temp;
	}

	public string getSkillName(){
		return skillName;
	}

	public void setST(bool temp){
		singleTarget = temp;
	}

	public bool getST(){
		return singleTarget;
	}

	public void setPassive(bool temp){
		passive = temp;
	}

	public bool getPassive(){
		return passive;
	}

	public void setRecurring(bool temp){
		recurring_effect = temp;
	}

	public bool getRecurring(){
		return recurring_effect;
	}

	public void setTriggerID(int temp){
		trigger_id = temp;
	}

	public int getTriggerID(){
		return trigger_id;
	}

	public void setEffectIcon(int temp){
		effect_icon = temp;
	}

	public int getEffectIcon(){
		return effect_icon;
	}
}
