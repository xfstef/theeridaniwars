﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class card : MonoBehaviour {

	persistentObjects persistent_data;
	GameObject my_frame, my_character, my_title, my_selection, times_selected, my_turn_meter, my_health, my_attack;
	List<GameObject> currentIcons = new List<GameObject> ();
	Image frame, character, selection, turn_meter_bkg;
	Text title, t_s_text, turn_m_text, health_text, attack_text;

	int card_id;	// Card id.
	string card_name;
	string full_name;
	int race = 0;	// 1 for Humans and 2 for Plati.
	int card_type = 0;	// 1 for Hero, 2 for Special Units, 3 for Units, 4 for Special Abilities, 5 for Abilities.
	int hero_class_offset;	// 0 for first class, 1 for second and 2 for third.
	int card_graphic = 0;	// Card Position within the sprite sheet.
	int card_code = 0;	// Card Position within the hero cards.
	int selected = 0;
	int max_selectable = 2;	// TODO: Adjust to match player workbench stats.
	int hero_only_portrait_id;	// Only used for hero cards.

	int attack_damage = 0;
	int life = 0;
	int original_life, original_damage;
	int turn_timer = -1;
	int speed_class;	// Used to define how fast the unit's turn will come again. Only used for units.
	int range_class;	// Used to define the range at which units can attack.

	skill skill_id;	// Used to define which skill this card represents.
	int[] affected_by;	// A list of all the current triggers affecting this card.

	bool is_instanced = false;
	int card_state = 0;	// 0 for in Deck, 1 for in Hand, 2 for picked up, 3 for Played, 4 for Destroyed.
	bool is_passive = false;	// Only true for some SAC.
	bool is_placed_down = false;
	bool is_enemy = false;
	int card_battle_state;	// 0 for Queued, 1 for Active, 2 for Disabled.
	int shaking = 0;
	Color normal_c = new Color (1.0f, 1.0f, 1.0f, 1.0f);
	Color damaged_c = new Color (1.0f, 0.25f, 0.0f, 1.0f);
	Color buffed_c = new Color (0.0f, 0.75f, 0.0f, 1.0f);

	void start(){
		if (!is_instanced) {
			GameObject go = GameObject.Find ("persistentGO");
			persistent_data = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
			is_instanced = true;
		}
	}

	public void setCardID(int new_id){
		card_id = new_id;
	}

	public int getCardID(){
		return card_id;
	}

	public void setName(String name){
		card_name = name;
	}

	public String getName(){
		return card_name;
	}

	public void setFullName(String name){
		full_name = name;
	}

	public String getFullName(){
		return full_name;
	}

	public void setRace(int race){
		this.race = race;
	}
	public int getRace(){
		return race;
	}

	public void setCardType(int type){
		card_type = type;
	}
	public int getCardType(){
		return card_type;
	}

	public void setHeroClassOffset(int type){
		hero_class_offset = type;
	}
	public int getHeroClassOffset(){
		return hero_class_offset;
	}

	public void setCardCode(int code){
		card_code = code;
	}
	public int getCardCode(){
		return card_code;
	}

	public void setPlacedDown(bool state){
		is_placed_down = state;
	}

	public bool getPlacedDown(){
		return is_placed_down;
	}

	public void setCardGraphic(int code){
		card_graphic = code;
		//Debug.Log ("Added card graphic code: " + card_graphic);
	}

	public int getCardGraphic(){
		return card_graphic;
	}

	public void setSelected(int setter){
		selected = setter;
	}

	public void incrementSelected(bool plus){
		if (plus)
			selected++;
		else
			selected--;
	}

	public int getSelected(){
		return selected;
	}

	public void setMaxSelected(int setter){
		max_selectable = setter;
	}

	public int getMaxSelected(){
		return max_selectable;
	}

	public void setHeroPortraitID(int setter){
		hero_only_portrait_id = setter;
	}

	public int getHeroPortraitID(){
		return hero_only_portrait_id;
	}

	public void setIsPassive(bool state){
		is_passive = state;
	}

	public bool getIsPassive(){
		return is_passive;
	}

	public void setHealth(int temp){
		life = temp;
	}

	public int getHealth(){
		return life;
	}

	public void setDamage(int temp){
		attack_damage = temp;
	}

	public int getDamage(){
		return attack_damage;
	}

	public void setRange(int temp){
		range_class = temp;
	}

	public int getRange(){
		return range_class;
	}

	public void setInitiative(int temp){
		speed_class = temp;
	}

	public int getInitiative(){
		return speed_class;
	}

	public void setBattleState(int state){
		card_battle_state = state;
	}

	public int getBattleState(){
		return card_battle_state;
	}

	public void setTurnTimer(int temp){
		turn_timer = temp;
	}

	public int getTurnTimer(){
		return turn_timer;
	}

	public void decrementTurnTimer(int amount){
		turn_timer -= amount;
	}

	public void setIsEnemy(){
		is_enemy = true;
	}

	public void setNotEnemey(){
		is_enemy = false;
	}

	public bool getIsEnemy(){
		return is_enemy;
	}

	public void setSkill(skill temp){
		skill_id = temp;
	}

	public skill getSkill(){
		return skill_id;
	}

	public void setShaker(int temp){
		shaking = temp;
	}

	public int getShaker(){
		return shaking;
	}

	public void decrementShaker(){
		shaking--;
	}

	public int cardHit(int damage, bool melee){
		int retaliation_damage = 0;

		if (melee)
			retaliation_damage = attack_damage;

		life -= damage;
		health_text.text = life.ToString();
		health_text.color = damaged_c;

		return retaliation_damage;
	}

	public bool retaliatedAgainst(int damage){
		bool still_alive = true;

		if (life - damage <= 0)
			still_alive = false;
		
		life -= damage;
		health_text.text = life.ToString();
		health_text.color = damaged_c;

		return still_alive;
	}

	public void healOrBuff(int plus_life, int plus_damage){

		life += plus_life;
		if (life == original_life) {
			health_text.text = life.ToString();
			health_text.color = normal_c;
		}
		if (life > original_life) {
			health_text.text = life.ToString();
			health_text.color = buffed_c;
		}
		if (life < original_life) {
			health_text.text = life.ToString();
			health_text.color = damaged_c;
		}

		attack_damage += plus_damage;
		if(attack_damage == original_damage) {
			attack_text.text = attack_damage.ToString();
			attack_text.color = normal_c;
		}
		if (attack_damage > original_damage) {
			attack_text.text = attack_damage.ToString();
			attack_text.color = buffed_c;
		}
		if (attack_damage < original_damage) {
			attack_text.text = attack_damage.ToString();
			attack_text.color = damaged_c;
		}
	}

	public void updateGraphics(){
		//start ();

		if (!is_instanced) {
			GameObject go = GameObject.Find ("persistentGO");
			persistent_data = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
			is_instanced = true;
		}

		my_frame = gameObject.transform.Find ("frame").gameObject;
		frame = my_frame.GetComponent (typeof(Image)) as Image;

		my_character = gameObject.transform.Find ("character").gameObject;
		character = my_character.GetComponent (typeof(Image)) as Image;

		my_title = gameObject.transform.Find ("frame/title").gameObject;
		title = my_title.GetComponent (typeof(Text)) as Text;

		my_selection = gameObject.transform.Find ("selected").gameObject;
		selection = my_selection.GetComponent (typeof(Image)) as Image;

		times_selected = gameObject.transform.Find ("selected/amount").gameObject;
		t_s_text = times_selected.GetComponent (typeof(Text)) as Text;

		updateSelected ();

		if (race == 1) {
			frame.sprite = persistent_data.getHumanFrame ();
			selection.sprite = persistent_data.getSelectedHumans ();
		} else {
			frame.sprite = persistent_data.getPlatiFrame ();
			selection.sprite = persistent_data.getSelectedPlati ();
		}
		//Debug.Log ("Updating card graphic: " + card_graphic);
		switch(card_type){
		case 1:	// Heros
			//title.text = persistent_data.getHeroNames () [card_code];
			//title.text = persistent_data.getTitle (card_code, -1 + race);
			title.text = card_name;
			character.sprite = persistent_data.getSprite (card_graphic, -1 + race);
			break;
		case 2:	// Special Units
			//title.text = persistent_data.getSpecialUnitNames () [card_code];
			//title.text = persistent_data.getTitle (card_code, 1 + race);
			title.text = card_name;
			character.sprite = persistent_data.getSprite (card_graphic, 1 + race);
			break;
		case 3:	// Units
			//title.text = persistent_data.getUnitNames () [card_code];
			//title.text = persistent_data.getTitle (card_code, 3 + race);
			title.text = card_name;
			character.sprite = persistent_data.getSprite (card_graphic, 3 + race);
			break;
		case 4:	// Special Abilities
			//title.text = persistent_data.getSACNames () [card_code];
			//title.text = persistent_data.getTitle (card_code, 5 + race);
			title.text = card_name;
			character.sprite = persistent_data.getSprite (card_graphic, 5 + race);
			break;
		case 5:	// Abilities
			//title.text = persistent_data.getAbilityNames () [card_code];
			//title.text = persistent_data.getTitle (card_code, 7 + race);
			title.text = card_name;
			character.sprite = persistent_data.getSprite (card_graphic, 7 + race);
			break;
		}
	}

	public void updateBattleGraphics(){

		if (!is_instanced) {
			GameObject go = GameObject.Find ("persistentGO");
			persistent_data = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
			is_instanced = true;
		}

		my_frame = gameObject.transform.Find ("frame").gameObject;
		frame = my_frame.GetComponent (typeof(Image)) as Image;
		frame.sprite = persistent_data.getBattleFrame (race);

		health_text = my_frame.transform.Find ("health").GetComponent<Text> ();
		health_text.text = life.ToString();

		attack_text = my_frame.transform.Find ("attack").GetComponent<Text> ();
		attack_text.text = attack_damage.ToString();

		my_character = gameObject.transform.Find ("character").gameObject;
		character = my_character.GetComponent (typeof(Image)) as Image;
		switch(card_type){
		case 1:	// Heros
			character.sprite = persistent_data.getSprite (card_graphic, -1 + race);
			break;
		case 2:	// Special Units
			character.sprite = persistent_data.getSprite (card_graphic, 1 + race);
			break;
		case 3:	// Units
			character.sprite = persistent_data.getSprite (card_graphic, 3 + race);
			break;
		case 4:	// Special Abilities
			character.sprite = persistent_data.getSprite (card_graphic, 5 + race);
			break;
		case 5:	// Abilities
			character.sprite = persistent_data.getSprite (card_graphic, 7 + race);
			break;
		}

		my_turn_meter = gameObject.transform.Find ("turnMeter").gameObject;
		turn_meter_bkg = my_turn_meter.GetComponent (typeof(Image)) as Image;
		turn_meter_bkg.sprite = persistent_data.getTurnCircle (race, card_battle_state);
		turn_m_text = my_turn_meter.transform.Find ("turnText").GetComponent<Text>();

	}

	public void updateTurnMeter(){
		if (turn_timer > 0) {
			turn_m_text.text = turn_timer.ToString ();
		}
		if (turn_timer == 0) {
			turn_m_text.text = "";
			card_battle_state = 1;
		}
		if (turn_timer < 0) {
			turn_m_text.text = "";
			card_battle_state = 2;
		}

		turn_meter_bkg.sprite = persistent_data.getTurnCircle (race, card_battle_state);
	}

	public void updateStats(){
		health_text.text = life.ToString();
		attack_text.text = attack_damage.ToString();
	}

	public void selectHero(){
		persistent_data.setSelectedHero (card_code);
		SceneManager.LoadScene ("newDeckAbility");
	}

	public void selectSAC(){
		persistent_data.setSelectedSAC (card_code);
		SceneManager.LoadScene ("deckView");
	}

	public void updateSelected(){
		if (selected > 0) {
			selection.enabled = true;
			t_s_text.text = ("x" + selected.ToString ());
		} else {
			selection.enabled = false;
			t_s_text.text = ("");
		}
	}

	public void setCardState(int new_state){
		card_state = new_state;
	}

	public int getCardState(){
		return card_state;
	}

	public void appropriateCard(){
		//start ();

		if (!is_instanced) {
			GameObject go = GameObject.Find ("persistentGO");
			persistent_data = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
			//my_frame = new GameObject ();
			//my_character = new GameObject ();
			//my_title = new GameObject ();
			//my_selection = new GameObject ();
			is_instanced = true;
		}

		card target_card = persistent_data.retrieveCard (card_id);
		//Debug.Log ("Card retrieved: " + target_card.getCardID());

		card_name = target_card.getName();
		full_name = target_card.getFullName();
		race = target_card.getRace();	// 1 for Humans and 2 for Plati.
		card_type = target_card.getCardType();	// 1 for Hero, 2 for Special Units, 3 for Units, 4 for Special Abilities, 5 for Abilities.
		card_graphic = target_card.getCardGraphic();	// Card Position within the sprite sheet.
		card_code = target_card.getCardCode();	// Card Position within the hero cards.
		is_passive = target_card.getIsPassive();	// Defining passive cards.
		card_state = target_card.getCardState();
		life = target_card.getHealth ();
		original_life = life;
		attack_damage = target_card.getDamage ();
		original_damage = attack_damage;
		speed_class = target_card.getInitiative ();
		range_class = target_card.getRange ();
		is_placed_down = target_card.is_placed_down;
		if (card_type == 4 || card_type == 5)
			skill_id = target_card.getSkill ();
	}

	public void appropriateInitiative(int btf_age){
		switch (card_type) {
		case 1:	// Hero Card
			speed_class += btf_age;
			break;
		case 2:	// Special Unit Type
			speed_class -= 100;
			speed_class += btf_age;
			break;
		case 3:	// Unit Type
			speed_class -= 200;
			speed_class += btf_age;
			break;
		}
		//Debug.Log ("Card type: " + card_type + ", Initiative: " + speed_class);
	}

	public void addEffectIcon(skill the_skill){
		GameObject new_icon = Instantiate (Resources.Load("Prefabs/passiveIcon"), transform.position, Quaternion.identity) as GameObject;
		new_icon.GetComponent<Image> ().sprite = persistent_data.getSprite (the_skill.getEffectIcon(), 11);
		new_icon.transform.SetParent (my_frame.transform);
		currentIcons.Add (new_icon);
		new_icon.transform.localPosition = new Vector2 (-70, 140 - currentIcons.Count * 50);
	}
}
