﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class player {

	deck[] the_decks;

	// Use this for initialization
	void Start () {
	
	}

	public deck[] getDecks(){
		return the_decks;
	}

	public void initializeEmpty(){
		the_decks = new deck[16];
		for (int x = 0; x < 16; x++) {
			deck new_deck = new deck();
			new_deck.setDeckSlot (x);
			the_decks[x] = new_deck;
		}
	}

}
