﻿using UnityEngine;
using System.Collections;

public class battleSlot : MonoBehaviour {
	bool card_present = false;
	public Vector2 x_y;
	public int id;

	public void setCardPresent(bool to_set){
		card_present = to_set;
	}

	public bool getCardPresent(){
		return card_present;
	}

	public Vector2 getXY(){
		return x_y;
	}

	public int getID(){
		return id;
	}
}
