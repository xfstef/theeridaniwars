﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class deckView : MonoBehaviour {

	persistentObjects decks_scripts;
	GameObject card_amount, commonTab, specialTab, spellTab, frame, bkg, mask, last_tab, last_selected_obj, plusB, minusB, deck_name;
	Text amount_selected;
	GameObject[] all_cards, tabs, commonCards, specialCards, skillsCards;
	card temp, hero_card, sac_card;
	int selected_slot, selected_race, selected_hero, selected_sac, nr_of_cards;
	float screen_width_adjuster, screen_height_adjuster;
	Image frameImage, bkg_image;
	player player_save;
	deck the_deck;

	public GameObject new_card;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
		bkg = GameObject.Find ("bkg");
		bkg_image = bkg.GetComponent<Image> ();
		GameObject.FindGameObjectWithTag ("CCV").GetComponent<Canvas> ().enabled = false;
		GameObject.FindGameObjectWithTag ("CCP").GetComponent<Canvas> ().enabled = false;
		mask = GameObject.Find ("mask") as GameObject;
		card_amount = GameObject.Find ("DeckCards");
		amount_selected = card_amount.GetComponent<Text> ();
		deck_name = GameObject.Find ("DeckName");

		minusB = GameObject.Find ("minusB");
		plusB = GameObject.Find ("plusB");

		screen_width_adjuster = (float)(Screen.width / 1920.0);
		screen_height_adjuster = (float)(Screen.height / 1080.0);

		Debug.Log ("Screen sizes: " + screen_width_adjuster + ", " + screen_height_adjuster);

		if (decks_scripts != null) {
			selected_slot = decks_scripts.getSelectedSlot ();
			selected_race = decks_scripts.getSelectedRace ();
			selected_hero = decks_scripts.getSelectedHero ();
			selected_sac = decks_scripts.getSelectedSAC ();
			player_save = decks_scripts.getPlayerSave ();
			Debug.Log ("The selected slot was: " + selected_slot + ", and the selected race was: " + selected_race + 
				", selec_hero: " + selected_hero + ", selected_sac: " + selected_sac + ", deck mode: " + decks_scripts.getNewDeckMode());

			tabs = GameObject.FindGameObjectsWithTag ("deckViewTab");
			Array.Sort (tabs, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });
			commonTab = tabs [0];
			specialTab = tabs [2];
			spellTab = tabs [1];

			Debug.Log ("Common: " + commonTab.name + ", special: " + specialTab.name + ", spells: " + spellTab.name);

			specialTab.transform.Find ("Panel").gameObject.SetActive(false);
			spellTab.transform.Find ("Panel").gameObject.SetActive(false);
			last_tab = commonTab;

			frame = GameObject.Find ("FrameImage");
			frameImage = frame.GetComponent (typeof(Image)) as Image;
			if (selected_race == 1) {
				frameImage.sprite = Resources.Load ("rama Humans", typeof(Sprite)) as Sprite;
				bkg_image.sprite = decks_scripts.getHumanBkg ();
			} else {
				frameImage.sprite = Resources.Load ("rama Plati", typeof(Sprite)) as Sprite;
				bkg_image.sprite = decks_scripts.getPlatiBkg ();
			}

			buildCardExplorer ();
			card prefab_hero, prefab_sac;

			if (decks_scripts.getNewDeckMode ()) {				
				the_deck = new deck ();
				the_deck.setName ("New Deck");
				the_deck.setRace (selected_race);
				the_deck.initizalizeDeck ();
				the_deck.setAmountSelected (2);
				nr_of_cards = 2;

				GameObject temp_card_1 = GameObject.Find ("HCard");//(card)Instantiate (temp);

				card hero_card = temp_card_1.GetComponent<card> ();
			
				GameObject temp_card_2 = GameObject.Find ("SACard");//card sac_card = GameObject.Find("SACCard");//(card)Instantiate (temp);

				card sac_card = temp_card_2.GetComponent<card> ();

				if (selected_race == 1) {
					prefab_hero = decks_scripts.getHumanCard(1, selected_hero);
					prefab_sac = decks_scripts.getHumanCard (4, selected_sac);
				} else {
					prefab_hero = decks_scripts.getPlatiCard(1, selected_hero);
					prefab_sac = decks_scripts.getPlatiCard (4, selected_sac);
				}

				hero_card.setCardID (prefab_hero.getCardID ());
				hero_card.setName (prefab_hero.getName ());
				hero_card.setRace (prefab_hero.getRace ());
				hero_card.setCardType (prefab_hero.getCardType ());
				hero_card.setCardGraphic (prefab_hero.getCardGraphic ());
				hero_card.setCardCode (prefab_hero.getCardCode ());
				/*hero_card.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
					cardSelected(hero_card);
				});*/
				hero_card.updateGraphics ();
				the_deck.setCard (0, prefab_hero.getCardID());
				the_deck.setHeroPortrait (prefab_hero.getHeroPortraitID ());

				sac_card.setCardID (prefab_sac.getCardID ());
				sac_card.setName (prefab_sac.getName ());
				sac_card.setRace (prefab_sac.getRace ());
				sac_card.setCardType (prefab_sac.getCardType ());
				sac_card.setCardGraphic (prefab_sac.getCardGraphic ());
				sac_card.setCardCode (prefab_sac.getCardCode ());
				/*sac_card.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
					cardSelected(sac_card);
				});*/
				sac_card.updateGraphics ();
				the_deck.setCard (1, prefab_sac.getCardID());

				player_save.getDecks()[selected_slot] = the_deck;
				decks_scripts.saveDecks ();
			} else {
				the_deck = player_save.getDecks() [selected_slot];
				deck_name.GetComponent<InputField> ().text = the_deck.getName ();
				nr_of_cards = the_deck.getAmountSelected ();
				amount_selected.text = nr_of_cards + "/20";

				if (selected_race == 1) {
					prefab_hero = decks_scripts.getHumanCard(1, the_deck.getCard(0)%10);
					prefab_sac = decks_scripts.getHumanCard (4, the_deck.getCard(1)%10);
				} else {
					prefab_hero = decks_scripts.getPlatiCard(1, the_deck.getCard(0)%10);
					prefab_sac = decks_scripts.getPlatiCard (4, the_deck.getCard(1)%10);
				}

				GameObject temp_card_1 = GameObject.Find ("HCard");//(card)Instantiate (temp);

				card hero_card = temp_card_1.GetComponent<card> ();

				GameObject temp_card_2 = GameObject.Find ("SACard");//card sac_card = GameObject.Find("SACCard");//(card)Instantiate (temp);

				card sac_card = temp_card_2.GetComponent<card> ();

				hero_card.setCardID (prefab_hero.getCardID ());
				hero_card.setName (prefab_hero.getName ());
				hero_card.setRace (prefab_hero.getRace ());
				hero_card.setCardType (prefab_hero.getCardType ());
				hero_card.setCardGraphic (prefab_hero.getCardGraphic ());
				hero_card.setCardCode (prefab_hero.getCardCode ());
				/*hero_card.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
					cardSelected(hero_card);
				});*/
				hero_card.updateGraphics ();

				sac_card.setCardID (prefab_sac.getCardID ());
				sac_card.setName (prefab_sac.getName ());
				sac_card.setRace (prefab_sac.getRace ());
				sac_card.setCardType (prefab_sac.getCardType ());
				sac_card.setCardGraphic (prefab_sac.getCardGraphic ());
				sac_card.setCardCode (prefab_sac.getCardCode ());
				/*sac_card.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
					cardSelected(sac_card);
				});*/
				sac_card.updateGraphics ();

				updateSelectedCards ();
			}
		}
	
	}

	void updateSelectedCards(){
		int temp_code;
		card temp_card;
		for (int x = 2; x < nr_of_cards; x++) {
			temp_code = the_deck.getCard (x) % 1000;
			switch (temp_code / 100) {
			case 2:	// Special Units.
				for (int y = 0; y < specialCards.Length; y++) {
					temp_card = specialCards [y].GetComponent<card>();
					if (temp_card.getCardID () == the_deck.getCard (x)) {
						temp_card.incrementSelected (true);
						temp_card.updateSelected ();
						break;
					}
				}
				break;
			case 3:	// Units.
				for (int y = 0; y < commonCards.Length; y++) {
					temp_card = commonCards [y].GetComponent<card>();
					if (temp_card.getCardID () == the_deck.getCard (x)) {
						temp_card.incrementSelected (true);
						temp_card.updateSelected ();
						break;
					}
				}
				break;
			case 5:	// Spells & Abilities.
				for (int y = 0; y < skillsCards.Length; y++) {
					temp_card = skillsCards [y].GetComponent<card>();
					if (temp_card.getCardID () == the_deck.getCard (x)) {
						temp_card.incrementSelected (true);
						temp_card.updateSelected ();
						break;
					}
				}
				break;
			}
		}
	}

	void refreshDeckData(){
		the_deck.setName (deck_name.GetComponent<InputField>().text);
		the_deck.setAmountSelected (nr_of_cards);
		Debug.Log ("Amount selected: " + nr_of_cards);
		int current = 2;

		for (int y = 0; y < commonCards.Length; y++) {
			switch(commonCards [y].GetComponent<card> ().getSelected ()) {
			case 0:
				break;
			case 1:
				the_deck.setCard (current, commonCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			case 2:
				the_deck.setCard (current, commonCards [y].GetComponent<card> ().getCardID ());
				current++;
				the_deck.setCard (current, commonCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			}
		}
		for (int y = 0; y < specialCards.Length; y++) {
			switch(specialCards [y].GetComponent<card> ().getSelected ()) {
			case 0:
				break;
			case 1:
				the_deck.setCard (current, specialCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			case 2:
				the_deck.setCard (current, specialCards [y].GetComponent<card> ().getCardID ());
				current++;
				the_deck.setCard (current, specialCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			}
		}
		for (int y = 0; y < skillsCards.Length; y++) {
			switch(skillsCards [y].GetComponent<card> ().getSelected ()) {
			case 0:
				break;
			case 1:
				the_deck.setCard (current, skillsCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			case 2:
				the_deck.setCard (current, skillsCards [y].GetComponent<card> ().getCardID ());
				current++;
				the_deck.setCard (current, skillsCards [y].GetComponent<card> ().getCardID ());
				current++;
				break;
			}
		}
		for (int x = current; x < 20; x++) {
			the_deck.setCard (x, 0);
		}
	}

	void BackPressed(){
		refreshDeckData ();
		decks_scripts.saveDecks ();
		SceneManager.LoadScene ("deckScreen");
	}

	void buildCardExplorer (){

		commonCards = new GameObject[10];
		specialCards = new GameObject[6];
		skillsCards = new GameObject[5];
		Vector2 start_pos;
		float horizontal_spacing = (float)(336 * screen_width_adjuster);

		if (commonCards.Length > 8) {
			RectTransform common_slide = (commonTab.transform.Find ("Panel/Scroll View/Viewport/Content")) as RectTransform;
			common_slide.sizeDelta = new Vector2 ((float)((Mathf.CeilToInt((commonCards.Length - 8)/2) * 280) * screen_width_adjuster), common_slide.sizeDelta.y);
			start_pos = getTabFirstPosition ();
		} else
			start_pos = getTabFirstPosition ();

		for (int x = 0; x < 10; x++) {
			card temp_card, prefab;
			GameObject new_card_clone = Instantiate (new_card, transform.position, Quaternion.identity) as GameObject;
			new_card_clone.transform.SetParent (commonTab.transform.Find("Panel/Scroll View/Viewport/Content/CanvasSlide"));
			new_card_clone.transform.localScale = new Vector3 (screen_width_adjuster, screen_height_adjuster, 0);
			if (x % 2 == 0) {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.y = start_pos.y * (-1);
			} else {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.x += horizontal_spacing;
				start_pos.y = start_pos.y * (-1);
			}

			if (selected_race == 1) {
				prefab = decks_scripts.getHumanCard(3, x);
			} else {
				prefab = decks_scripts.getPlatiCard(3, x);
			}

			temp_card = new_card_clone.GetComponent<card> ();
			temp_card.setCardID (prefab.getCardID ());
			temp_card.setName (prefab.getName ());
			temp_card.setRace (selected_race);
			temp_card.setCardType (prefab.getCardType ());
			temp_card.setCardGraphic (prefab.getCardGraphic ());
			temp_card.setCardCode (prefab.getCardCode ());
			new_card_clone.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
				cardSelected(new_card_clone, temp_card);
			});
			temp_card.updateGraphics ();

			commonCards [x] = new_card_clone;
		}
		start_pos = getTabFirstPosition ();
		for (int x = 0; x < 6; x++) {
			card temp_card, prefab;
			GameObject new_card_clone = Instantiate (new_card, transform.position, Quaternion.identity) as GameObject;
			new_card_clone.transform.SetParent (specialTab.transform.Find("Panel/Scroll View/Viewport/Content/CanvasSlide"));
			new_card_clone.transform.localScale = new Vector3 (screen_width_adjuster, screen_height_adjuster, 0);
			if (x % 2 == 0) {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.y = start_pos.y * (-1);
			} else {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.x += horizontal_spacing;
				start_pos.y = start_pos.y * (-1);
			}

			if (selected_race == 1) {
				prefab = decks_scripts.getHumanCard(2, x);
			} else {
				prefab = decks_scripts.getPlatiCard(2, x);
			}

			temp_card = new_card_clone.GetComponent<card> ();
			temp_card.setCardID (prefab.getCardID ());
			temp_card.setName (prefab.getName ());
			temp_card.setRace (selected_race);
			temp_card.setCardType (prefab.getCardType ());
			temp_card.setCardGraphic (prefab.getCardGraphic ());
			temp_card.setCardCode (prefab.getCardCode ());
			new_card_clone.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
				cardSelected(new_card_clone, temp_card);
			});
			temp_card.updateGraphics ();

			specialCards [x] = new_card_clone;
		}
		start_pos = getTabFirstPosition ();
		for (int x = 0; x < 5; x++) {
			card temp_card, prefab;
			GameObject new_card_clone = Instantiate (new_card, transform.position, Quaternion.identity) as GameObject;
			new_card_clone.transform.SetParent (spellTab.transform.Find("Panel/Scroll View/Viewport/Content/CanvasSlide"));
			new_card_clone.transform.localScale = new Vector3 (screen_width_adjuster, screen_height_adjuster, 0);
			if (x % 2 == 0) {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.y = start_pos.y * (-1);
			} else {
				new_card_clone.transform.localPosition = start_pos;
				start_pos.x += horizontal_spacing;
				start_pos.y = start_pos.y * (-1);
			}

			if (selected_race == 1) {
				prefab = decks_scripts.getHumanCard(5, x);
			} else {
				prefab = decks_scripts.getPlatiCard(5, x);
			}

			temp_card = new_card_clone.GetComponent<card> ();
			temp_card.setCardID (prefab.getCardID ());
			temp_card.setName (prefab.getName ());
			temp_card.setRace (selected_race);
			temp_card.setCardType (prefab.getCardType ());
			temp_card.setCardGraphic (prefab.getCardGraphic ());
			temp_card.setCardCode (prefab.getCardCode ());
			new_card_clone.transform.Find("frame").GetComponent<Button>().onClick.AddListener(delegate() {
				cardSelected(new_card_clone, temp_card);
			});
			temp_card.updateGraphics ();

			skillsCards [x] = new_card_clone;
		}
	}

	void commonSelected(){
		commonTab.transform.Find ("Panel").gameObject.SetActive(true);
		specialTab.transform.Find ("Panel").gameObject.SetActive(false);
		spellTab.transform.Find ("Panel").gameObject.SetActive(false);
		frame.transform.SetAsLastSibling ();
		mask.transform.SetAsLastSibling ();
		last_tab = commonTab;
	}

	void specialSelected(){
		commonTab.transform.Find ("Panel").gameObject.SetActive(false);
		specialTab.transform.Find ("Panel").gameObject.SetActive(true);
		spellTab.transform.Find ("Panel").gameObject.SetActive(false);
		frame.transform.SetAsLastSibling ();
		mask.transform.SetAsLastSibling ();
		last_tab = specialTab;
	}

	void skillsSelected(){
		commonTab.transform.Find ("Panel").gameObject.SetActive(false);
		specialTab.transform.Find ("Panel").gameObject.SetActive(false);
		spellTab.transform.Find ("Panel").gameObject.SetActive(true);
		frame.transform.SetAsLastSibling ();
		mask.transform.SetAsLastSibling ();
		last_tab = spellTab;
	}

	Vector2 getTabFirstPosition(){
		Vector2 result = new Vector2 ();
		float first_card_x = -528;
		float first_card_y = 216;

		result.x = (float)(first_card_x * screen_width_adjuster);
		result.y = (float)(first_card_y * screen_height_adjuster);

		return result;
	}

	void handlePlusButton(card target_card){
		if (nr_of_cards < 20 && target_card.getMaxSelected() > target_card.getSelected()) {
			plusB.SetActive (true);
		} else {
			plusB.SetActive (false);
		}
	}

	void handleMinusButton(card target_card){
		if (nr_of_cards > 2 && target_card.getSelected() > 0) {
			minusB.SetActive (true);
		} else {
			minusB.SetActive (false);
		}
	}

	void cardSelected(GameObject selected_obj, card selected){
		last_selected_obj = selected_obj;
		Debug.Log ("Card selected: " + selected_obj.GetComponent<card>().getName() + ", selected: " + selected_obj.GetComponent<card>().getSelected());
		commonTab.transform.Find ("Panel").gameObject.SetActive(false);
		specialTab.transform.Find ("Panel").gameObject.SetActive(false);
		spellTab.transform.Find ("Panel").gameObject.SetActive(false);
		mask.GetComponent<Image> ().enabled = true;
		GameObject.Find ("CanvasDeckView").GetComponent<CanvasGroup> ().interactable = false;
		GameObject.FindGameObjectWithTag ("CCV").GetComponent<Canvas> ().enabled = true;

		GameObject selected_card = GameObject.Find ("sCard");
		card the_card = selected_card.GetComponent<card> ();
		the_card.setCardID (selected.getCardID ());
		the_card.setName (selected.getName ());
		the_card.setRace (selected_race);
		the_card.setCardType (selected.getCardType ());
		the_card.setCardGraphic (selected.getCardGraphic ());
		the_card.setCardCode (selected.getCardCode ());
		the_card.setSelected (selected.getSelected ());
		the_card.setMaxSelected (selected.getMaxSelected ());

		the_card.updateGraphics ();


		plusB.GetComponent<Button> ().onClick.AddListener (delegate() {
			modifySelection (true, the_card);
		});
		minusB.GetComponent<Button> ().onClick.AddListener (delegate() {
			modifySelection (false, the_card);
		});
		handlePlusButton (the_card);
		handleMinusButton (the_card);
		GameObject.Find ("closeB").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("closeB").GetComponent<Button> ().onClick.AddListener (delegate() {
			closeSelection (the_card);
		});
	}

	void modifySelection(Boolean plus, card sel_card){
		Debug.Log ("Modify called on " + plus);
		if (plus) {
			if (sel_card.getSelected () < sel_card.getMaxSelected ()) {
				sel_card.incrementSelected (true);
				//sel_card.setSelected (sel_card.getSelected () + 1);
				nr_of_cards++;
				handlePlusButton (sel_card);
				handleMinusButton (sel_card);
			} else {
				return;
			}
		} else if (sel_card.getSelected () > 0) {
			sel_card.incrementSelected (false);
			//sel_card.setSelected (sel_card.getSelected () - 1);
			nr_of_cards--;
			handlePlusButton (sel_card);
			handleMinusButton(sel_card);
		} else
			return;
		
		sel_card.updateSelected ();
		amount_selected.text = nr_of_cards + "/20";
	}

	void closeSelection(card selection_card){
		Debug.Log ("Closing Card: " + selection_card.getName() + ", selected: " + selection_card.getSelected());
		mask.GetComponent<Image> ().enabled = false;
		GameObject.Find ("CanvasDeckView").GetComponent<CanvasGroup> ().interactable = true;
		GameObject.FindGameObjectWithTag ("CCV").GetComponent<Canvas> ().enabled = false;
		switch (last_tab.name) {
		case "Tab Panel - CUnits":
			commonSelected ();
			break;
		case "Tab Panel - SUnits":
			specialSelected ();
			break;
		case "Tab Panel - Spells":
			skillsSelected ();
			break;
		}
		minusB.GetComponent<Button> ().onClick.RemoveAllListeners ();
		plusB.GetComponent<Button> ().onClick.RemoveAllListeners ();

		last_selected_obj.GetComponent<card> ().setSelected (selection_card.getSelected ());
		last_selected_obj.GetComponent<card> ().updateSelected ();


		//the_deck - Update the_deck !!!
	}

	public void proceed(){
		refreshDeckData ();
		decks_scripts.saveDecks ();

		if (the_deck.getAmountSelected () != 20)
			return;

		commonTab.transform.Find ("Panel").gameObject.SetActive(false);
		specialTab.transform.Find ("Panel").gameObject.SetActive(false);
		spellTab.transform.Find ("Panel").gameObject.SetActive(false);
		mask.GetComponent<Image> ().enabled = true;
		GameObject.Find ("CanvasDeckView").GetComponent<CanvasGroup> ().interactable = false;
		GameObject.FindGameObjectWithTag ("CCP").GetComponent<Canvas> ().enabled = true;

		GameObject.Find ("closeCCPB").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("closeCCPB").GetComponent<Button> ().onClick.AddListener (delegate() {
			closeProceed ();
		});

		GameObject.Find ("quickButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("quickButton").GetComponent<Button> ().onClick.AddListener (delegate() {
			selectMode (0);
		});

		// TODO: Add other game modes.
	}

	void closeProceed(){
		mask.GetComponent<Image> ().enabled = false;
		GameObject.Find ("CanvasDeckView").GetComponent<CanvasGroup> ().interactable = true;
		GameObject.FindGameObjectWithTag ("CCP").GetComponent<Canvas> ().enabled = false;
		switch (last_tab.name) {
		case "Tab Panel - CUnits":
			commonSelected ();
			break;
		case "Tab Panel - SUnits":
			specialSelected ();
			break;
		case "Tab Panel - Spells":
			skillsSelected ();
			break;
		}
	}

	void selectMode(int mode){
		// TODO: Build separate game modes selection.
		switch (mode) {
		case 0:
			SceneManager.LoadScene ("battlefield");
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		}
	}

	void Update () {
		var pointer = new PointerEventData(EventSystem.current);
		if (Input.GetKeyDown (KeyCode.Escape))
			if (GameObject.Find ("CanvasDeckView").GetComponent<CanvasGroup> ().interactable == true)
				BackPressed ();
			else if(GameObject.FindGameObjectWithTag ("CCV").GetComponent<Canvas> ().enabled == true)
				ExecuteEvents.Execute(GameObject.Find ("closeB").GetComponent<Button> ().gameObject, pointer, ExecuteEvents.submitHandler);
			else if(GameObject.FindGameObjectWithTag ("CCP").GetComponent<Canvas> ().enabled == true)
				ExecuteEvents.Execute(GameObject.Find ("closeCCPB").GetComponent<Button> ().gameObject, pointer, ExecuteEvents.submitHandler);
	}
}
