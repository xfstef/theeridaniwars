﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class deckSlot : MonoBehaviour {

	persistentObjects decks_scripts;
	GameObject deckPortrait, deckFrame, deckTitle, slotText;
	Image d_portrait, d_frame;
	Text d_title, s_text;

	public int my_id = 0;
	deck my_deck = null;

	void Start () {
		deckPortrait = new GameObject ();
		deckFrame = new GameObject ();
		deckTitle = new GameObject ();
		slotText = new GameObject ();

		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
		if (decks_scripts != null) {
			decks_scripts.setSelectedSlot (0);
			my_deck = decks_scripts.getDeckBySlot (my_id);
		}
	}

	public void updateGraphics() {
		Start ();

		deckPortrait = gameObject.transform.Find ("DeckPortrait").gameObject;
		d_portrait = deckPortrait.GetComponent (typeof(Image)) as Image;
		deckFrame = gameObject.transform.Find ("DeckFrame").gameObject;
		d_frame = deckFrame.GetComponent (typeof(Image)) as Image;
		deckTitle = deckFrame.transform.Find ("DeckTitle").gameObject;
		d_title = deckTitle.GetComponent (typeof(Text)) as Text;
		slotText = deckFrame.transform.Find ("SlotText").gameObject;
		s_text = slotText.GetComponent (typeof(Text)) as Text;

		if (my_deck.getRace() == 0) {
			Debug.Log ("Setting up new deck on slot:" + my_id);
		} else {
			// TODO: Load the deck hero graphics and name to the deck slot.
			Debug.Log ("Deck exists on slot: " + my_id + ", named: " + my_deck.getName() + ", race: " + my_deck.getRace());
			if (my_deck.getRace () == 1)
				d_frame.sprite = decks_scripts.getHumanDeckFrame ();
			else
				d_frame.sprite = decks_scripts.getPlatiDeckFrame ();
			s_text.text = "";
			d_title.text = my_deck.getName ();
			d_portrait.sprite = decks_scripts.getHeroPortrait (my_deck.getHeroPortrait ());
		}
	}

	public void SetSelectedSlot(){
		
		decks_scripts.setSelectedSlot (my_id);
		if (my_deck.getRace() == 0) {
			decks_scripts.setNewDeckMode (true);
			SceneManager.LoadScene ("newDeck");
		}
		else {
			//TODO: Set the selected Race, Hero and SAC.
			decks_scripts.setNewDeckMode (false);
			decks_scripts.setSelectedRace (my_deck.getRace ());
			decks_scripts.setSelectedHero (my_deck.getCard (0) % 10);
			decks_scripts.setSelectedSAC (my_deck.getCard (1) % 10);
			SceneManager.LoadScene ("deckView");
		}
	}
}
