﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class newDeck : MonoBehaviour {

	persistentObjects decks_scripts;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;
		if (decks_scripts != null) {
			int selected_slot;
			selected_slot = decks_scripts.getSelectedSlot();
			Debug.Log ("The selected slot was: " + selected_slot + ", deck mode: " + decks_scripts.getNewDeckMode());
			decks_scripts.setSelectedRace (0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			SceneManager.LoadScene ("deckScreen");
	}

	public void BackPressed(){
		SceneManager.LoadScene ("deckScreen");
	}
}
