﻿using UnityEngine;
using System.Collections;

public class skillLibrary {

	card[] battlefield_cards = new card[12]();

	void parseTrigger(int trigger_id){
		int trigger_class = trigger_id / 10;
		int sub_trigger = trigger_id % 10;

		switch (trigger_class) {
		case 1:	// Hero Triggers.
			switch (sub_trigger) {
			case 0:	// Hero death ward.
				break;
			case 1:	// Set invisibility to self.
				break;
			case 2:	// Hero Target receives one extra point of damage from other ranged attacks for one turn.
				break;
			case 3:	// Gives bonus to attack but causes self harm on cast.
				break;
			case 4:	// Hero gains extra retaliation damage.
				break;
			case 5:	// Hero bonus armor points.
				break;
			case 6:	// Hero melee attack may not trigger retaliation.
				break;
			case 7:	// Hero gets a critical strike (+50% damage) chance of 20%.
				break;
			}
			break;
		case 2:	// Direct Heals.
			switch (sub_trigger) {
			case 0:	// Direct heal to full health.
				break;
			}
			break;
		case 3:	// Periodic Heals.
			switch (sub_trigger) {
			case 0:	// Small heal over time to one target.
				break;
			case 1:	//	Periodic heal to all robots.
				break;
			}
			break;
		case 4:	// Extra damage.
			switch (sub_trigger) {
			case 0:	//	%Chance to crit added. 
				break;
			case 2:	// Extra retaliation damage + extra effect against mechanical.
				break;
			case 3:	// Steam Units gain +2 attack.
				break;
			case 4:	// All melee units gain extra damage on their first strike.
				break;
			case 5:	// Damage debuff.
				break;
			}
			break;
		case 5:	// Summoning.
			switch (sub_trigger) {
			case 0:	// Summons a friendly random Hollow One.
				break;
			case 1:	// All current units get Hollow Brother respawn but lose one health point.
				break;
			case 2:	// Unit skips ahead and moves next.
				break;
			}
			break;
		case 6:	// Invincibilities and taunting.
			switch (sub_trigger) {
			case 0:	// Melee Invincibility for one round.
				break;
			case 1:	// Permanent Taunt.
				break;
			case 2:	// Target skips its turn automatically.
				break;
			case 3:	// The next hero attack will stun the target for one turn.
				break;
			}
			break;
		case 7:	// Special Unit Effects.
			switch (sub_trigger) {
			case 0:	// Hero gains Hollow One tamer ability + Hollow Ones get +attack for each extra Whip Master on the field.
				break;
			case 1:	// Choose between +attack, +health or +less of both.
				break;
			case 2:	// Unit retreats back to hand at the end of the turn.
				break;
			}
			break;
		case 8:	// Card Draw / Hand Effects.
			switch (sub_trigger) {
			case 0:	// Draw 3 cards. Pick one for your hand. The rest go back in the deck.
				break;
			case 1:	// Select a card to destroy / discard and summon a random unit from your deck either to the battlefield or in hand.
				break;
			}
			break;
		}
	}
}
