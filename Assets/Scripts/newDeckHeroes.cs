﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class newDeckHeroes : MonoBehaviour {

	persistentObjects decks_scripts;
	GameObject class1go, class2go, class3go, bkg;
	Text class1, class2, class3;
	Image bkg_image;
	GameObject[] all_cards;
	card prefab;

	// Use this for initialization
	void Start () {
		class1go = GameObject.Find ("Class1Text");
		class2go = GameObject.Find ("Class2Text");
		class3go = GameObject.Find ("Class3Text");
		class1 = class1go.GetComponent<Text> ();
		class2 = class2go.GetComponent<Text> ();
		class3 = class3go.GetComponent<Text> ();
		bkg = GameObject.Find ("bkg");
		bkg_image = bkg.GetComponent<Image> ();

		GameObject go = GameObject.Find ("persistentGO");
		decks_scripts = go.GetComponent (typeof(persistentObjects)) as persistentObjects;

		if (decks_scripts != null) {
			int selected_slot;
			int selected_race;
			selected_slot = decks_scripts.getSelectedSlot();
			selected_race = decks_scripts.getSelectedRace();
			Debug.Log ("The selected slot was: " + selected_slot + ", and the selected race was: " + selected_race);
			decks_scripts.setSelectedHero (0);
			prefab = gameObject.AddComponent<card> ();

			all_cards = GameObject.FindGameObjectsWithTag ("CardObject");
			Array.Sort (all_cards, delegate(GameObject card1, GameObject card2) { return card1.name.CompareTo(card2.name); });

			for (int x = 0; x < all_cards.Length; x++) {
				card temp;
				temp = all_cards [x].GetComponent<card> ();

				if (selected_race == 1) {
					prefab = decks_scripts.getHumanCard(1, x);
				} else {
					prefab = decks_scripts.getPlatiCard(1, x);
				}
				temp.setName (prefab.getName ());
				temp.setRace (prefab.getRace ());
				temp.setCardType (prefab.getCardType ());
				temp.setCardGraphic (prefab.getCardGraphic ());
				temp.setCardCode (prefab.getCardCode ());
				temp.updateGraphics ();
			}

			if (selected_race == 1) {
				class1.text = "Arms Assisted Security";
				class2.text = "Communications & HR";
				class3.text = "Engineering";
				bkg_image.sprite = decks_scripts.getHumanBkg ();
			} else {
				class1.text = "Whitened Sun";
				class2.text = "Free Traders";
				class3.text = "The Young Brotherhood";
				bkg_image.sprite = decks_scripts.getPlatiBkg ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			SceneManager.LoadScene ("newDeck");
	}

	public void BackPressed(){
		decks_scripts.setSelectedRace (0);
		SceneManager.LoadScene ("newDeck");
	}
}
